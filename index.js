const Hapi = require('hapi');
const fs = require('fs');

const config = require('./config/quest');
const swagger = require('./config/swagger');
const good = require('./config/good');

const models = require('./app/models');

const auth = (request, decoded, callback) => {

    if (!decoded) {
        return callback(null, false, decoded);
    }
    return callback(null, true, decoded);
};

const key = fs.readFileSync(config.key);
const server = new Hapi.Server();
server.connection(config.connection);

server.register([
{
    register: require('vision')
}, {
    register: require('inert')
}, {
    register: require('hapi-swagger'),
    options: swagger.options
}, {
    register: require('good'),
    options: good
}, {
    register: require('blipp'),
    options: {
        showAuth: true,
        showStart: config.log
    }
}, {
    register: require('hapi-router'),
    options: { routes: './app/routes/*.js' }
}, {
    register: require('hapi-auth-jwt')
}], (err) => {
    if (err) {
        console.log('Error registering hapi plugins');
        throw err;
    }
    server.auth.strategy('jwt', 'jwt', 'required', {
        validateFunc: auth,
        key: key
    });
});

models.sequelize.sync().then(() => {
    server.start(() => {
        if (config.log) {
            console.log('Server running at: ' + server.info.uri);
        }
    });
});

module.exports = server;
