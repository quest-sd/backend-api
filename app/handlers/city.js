const boom = require('boom');
const City = require('../models').City;
const responses = require('../../config/swagger').responses.city;
const uuid = require('node-uuid');

const city = {
    getCities: (req, reply) => {
        City.findAll().then(cities => {
            reply(cities);
        }).catch(err => {
            reply(boom.badImplementation(responses.get.responses['500'].description), err);
        });
    },
    addCity: (req, reply) => {
        City.create({
            id: uuid.v4(),
            name: req.payload.name
        }).then(city => {
            reply(city).code(201);
        }).catch(err => {
            if (err.name === 'SequelizeUniqueConstraintError') {
                reply(boom.conflict(responses.post.responses['409'].description));
            } else {
                reply(boom.badImplementation(responses.post.responses['500'].description), err);
            }
        });
    },
    updateCity: (req, reply) => {
        City.update({
            name: req.payload.name
        }, {
            where: {
                id: req.payload.id
            }
        }).then(result => {
            if (result[0] == 1) {
                City.findById(req.payload.id).then(city => {
                    reply(city);
                }).catch(err => {
                    reply(boom.badImplementation(responses.post.responses['500'].description));
                });
            } else {
                reply(boom.badImplementation(responses.post.responses['500'].description));
            }
        }).catch(err => {
            reply(boom.badImplementation(responses.post.responses['500'].description), err);
        });
    }
};

module.exports = city;
