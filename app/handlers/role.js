const boom = require('boom');
const uuid = require('node-uuid');
const Role = require('../models').Role;
const responses = require('../../config/swagger').responses.role;

const role = {
    getRoles: (req, reply) => {
        Role.findAll().then(roles => {
            reply(roles);
        }).catch(err => {
            reply(boom.badImplementation(responses.get.responses['500'].description), err);
        });
    },
    createRole: (req, reply) => {
        Role.create({
            id: uuid.v4(),
            name: req.payload.name
        }).then(role => {
            reply(role).code(201);
        }).catch(err => {
            if (err.name === 'SequelizeUniqueConstraintError') {
                reply(boom.conflict(responses.post.responses['409'].description));
            } else {
                reply(boom.badImplementation(responses.post.responses['500'].description), err);
            }
        });
    }
};

module.exports = role;
