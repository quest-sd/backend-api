const boom = require('boom');
const uuid = require('node-uuid');
const UserReport = require('../models').UserReport;
const responses = require('../../config/swagger').responses.report;

const role = {
	getReports: (req, reply) => {
		UserReport.findAll().then(reports => {
			reply(reports);
		}).catch(err => {
			reply(boom.badImplementation(responses.get.responses['500'].description), err);
		});
	},
	createReport: (req, reply) => {
		UserReport.create({
			reportId: uuid.v4(),
			reporterId: req.payload.reporterId,
			offenderId: req.payload.offenderId,
			timeStamp: new Date(),
			questId: req.payload.questId,
			status: 'UNRESOLVED',
			reason: req.payload.reason,
			message: req.payload.message,
		}).then(report => {
			reply(report).code(201);
		}).catch(err => {
			reply(boom.badImplementation(responses.post.responses['500'].description), err);
		});
	},
	resolveReport: (req, reply) => {
		UserReport.update({
			status: 'RESOLVED'
		}, {
			where: { reportId: req.params.id }
		}).then(result => {
			if (result[0] === 1) {
				reply().code(204);
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.put.responses['500'].description, err.name));
		});
	}
};

module.exports = role;
