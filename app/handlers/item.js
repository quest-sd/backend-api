const boom = require('boom');
const uuid = require('node-uuid');
const Item = require('../models').Item;
const config = require('../../config/quest');
const responses = require('../../config/swagger').responses.user;

const item = {
	addItem: (req, reply) => {
		Item.create({
			itemId: uuid.v4(),
			ownerId: req.auth.credentials.userId,
			imageId: req.payload.imageId,
			name: req.payload.name,
            description: req.payload.description,
            dateAdded: Date.now(),
            condition: req.payload.condition
		}).then(item => {
			reply(item).code(201);
		}).catch(err => {
			reply(boom.badImplementation(responses.post.responses['500'].description, err.name));
		});
	},
	deleteItem: (req, reply) => {
		Item.destroy({
			where: {
				itemId: req.params.id,
				ownerId: req.auth.credentials.userId
			}
		}).then(rows => {
			if (rows === 1) {
				reply().code(204);
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.delete.responses['500'].description, err.name));
		});
	},
	updateItem: (req, reply) => {
		Item.update(req.payload, {
			where: {
				itemId: req.payload.itemId,
				ownerId: req.auth.credentials.userId
			}
		}).then(result => {
			if (result[0] === 1) {
				reply().code(204);
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.put.responses['500'].description, err.name));
		});
	},
	getItem: (req, reply) => {
		Item.findOne({
			where: {
				itemId: req.params.itemId
			}
		}).then(item => {
			reply(item);
		}).catch(err => {
			reply(boom.badImplementation(responses.get.responses['500'].description), err);
		});
	},
	getItems: (req, reply) => {
		Item.findAll({
			where: {
				ownerId: req.auth.credentials.userId
			}
		}).then(items => {
			reply(items);
		}).catch(err => {
			reply(boom.badImplementation(responses.get.responses['500'].description), err);
		});
	}
};

module.exports = item;
