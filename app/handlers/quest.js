const boom = require('boom');
const uuid = require('node-uuid');
const Quest = require('../models').Quest;
const QuestTag = require('../models').QuestTag;
const QuestUser = require('../models').QuestUser;
const Item = require('../models').Item;
const User = require('../models').User;
const config = require('../../config/quest');
const notifier = require('../helpers/notification');
const responses = require('../../config/swagger').responses.quest;
const statuses = require('../../config/constants').statuses;

const calculateExp = (exp) => {
	return Math.floor(Math.sqrt(exp) * config.expConstant);
};

const quest = {
	createQuest: (req, reply) => {
		Quest.create({
			questId: uuid.v4(),
			giverId: req.auth.credentials.userId,
			name: req.payload.name,
			rewardItemId: req.payload.rewardItemId,
			rewardExp: 16,
			created: Date.now(),
			status: statuses.open,
			description: req.payload.description,
			address: req.payload.address,
			latitude: req.payload.latitude,
			longitude: req.payload.longitude,
			cityId: req.auth.credentials.cityId,
			cashReward: req.payload.cashReward
		}).then(quest => {
			reply(quest).code(201);
		}).catch(err => {
			reply(boom.badImplementation(responses.post.responses['500'].description, err.name));
		});
	},
	applyForQuest: (req, reply) => {
		QuestUser.create({
			userId: req.params.userId,
			questId: req.params.questId
		}).then(questUser => {
			reply().code(204);
		}).catch((err) => {
			reply(boom.badImplementation(responses.accept.responses['500'].description, err.name));
		});
	},
	acceptForQuest: (req, reply) => {
		Quest.update({
			receiverId: req.params.userId,
			status: statuses.inProgress
		}, {
			where: { questId: req.params.questId }
		}).then(() => {
			Quest.findOne({
				where: { questId: req.params.questId }
			}).then(quest => {
				User.findOne({
					where: { userId: req.params.userId }
				}).then(user => {
					const message = `"${quest.name}" has been added to your journal...`;
					notifier.sendNotification('Quest Accepted!', message, user.email);
					console.log('PUSH NOTIFICATION', message);
					reply().code(204);
				}).catch(err => {
					reply(boom.badImplementation(responses.accept.responses['500'].description, err.name));
				});
			})
		}).catch(err => {
			reply(boom.badImplementation(responses.accept.responses['500'].description, err.name));
		});
	},
	completeQuest: (req, reply) => {
		Quest.findOne({
			where: { questId: req.params.questId }
		}).then(quest => {
			let status;
			if (req.auth.credentials.userId === quest.receiverId) {
				status = statuses.awaitingQuestGiver;
			} else if (req.auth.credentials.userId === quest.giverId) {
				status = statuses.completed;
			} else {
				reply(boom.badImplementation(responses.complete.responses['500'].description, err.name));
			}
			Quest.update({
				status: status
			}, {
				where: { questId: req.params.questId }
			}).then(() => {
				if (req.auth.credentials.userId === quest.giverId) {
					User.findOne({
						where: { userId: quest.receiverId }
					}).then(user => {
						user.exp += quest.rewardExp;
						const prevLevel = user.level;
						user.level = calculateExp(user.exp);
						User.update({
							exp: user.exp,
							level: user.level
						}, {
							where: { userId: user.userId }
						}).then(() => {
							notifier.sendNotification('Quest Finished!', `Congratulations. You have finished the "${quest.name}" quest!!!!`, user.email);
							console.log('PUSH NOTIFICATION', `Quest Finished for user ${user.userId}.`);
							if (user.level > prevLevel) {
								const message = `Congratulations. You have leveled up to level ${user.level}!`;
								notifier.sendNotification('Level Up!!!', message, user.email);
								console.log('PUSH NOTIFICATION', message);
							}
							reply().code(204);
						}).catch(err => {
							reply(boom.badImplementation(responses.put.responses['500'].description, err.name));
						});
					}).catch(err => {
						reply(boom.badImplementation(responses.put.responses['500'].description, err.name));
					});
				} else {
					reply().code(204);
				}
			}).catch(err => {
				reply(boom.badImplementation(responses.complete.responses['500'].description, err.name));
			});
		}).catch(err => {
			reply(boom.badImplementation(responses.getQuest.responses['500'].description), err);
		});
	},
	abandonQuest: (req, reply) => {
		Quest.findOne({
			where: { questId: req.params.questId }
		}).then(quest => {
			if (req.auth.credentials.userId === quest.receiverId) {
				Quest.update({
					status: statuses.open,
					receiverId: null,
				}, {
					where: { questId: req.params.questId }
				}).then(() => {
					reply().code(204);
				}).catch(err => {
					reply(boom.badImplementation(responses.complete.responses['500'].description, err.name));
				});
			} else {
				reply(boom.badImplementation(responses.abandon.responses['500'].description, err.name));
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.getQuest.responses['500'].description), err);
		});
	},
	rateQuest: (req, reply) => {
		Quest.findOne({
			where: { questId: req.params.questId }
		}).then(quest => {
			let review;
			if (req.auth.credentials.userId === quest.receiverId) {
				review = {
					giverRating: req.payload.rate,
					giverComment: req.payload.comment
				};
			} else if (req.auth.credentials.userId === quest.giverId) {
				review = {
					receiverRating: req.payload.rate,
					receiverComment: req.payload.comment
				};
			} else {
				reply(boom.badImplementation(responses.abandon.responses['500'].description, err.name));
			}
			Quest.update(review, {
				where: { questId: req.params.questId }
			}).then(() => {
				reply().code(204);
			}).catch(err => {
				reply(boom.badImplementation(responses.complete.responses['500'].description, err.name));
			});
		}).catch(err => {
			reply(boom.badImplementation(responses.getQuest.responses['500'].description), err);
		});
	},
	updateQuest: (req, reply) => {
		Quest.update({
			name: req.payload.name,
			rewardItemId: req.payload.rewardItemId,
			description: req.payload.description,
			address: req.payload.address,
			latitude: req.payload.latitude,
			longitude: req.payload.longitude,
			cashReward: req.payload.cashReward
		}, {
			where: { questId: req.params.questId }
		}).then(() => {
			reply().code(204);
		}).catch(err => {
			reply(boom.badImplementation(responses.put.responses['500'].description, err.name));
		});
	},
	getQuests: (req, reply) => {
		Quest.findAll({
			attributes: ['questId', 'giverId', 'receiverId', 'name'],
			where: { cityId: req.auth.credentials.cityId }
		}).then(quests => {
			quests.forEach(quest => {
				quest.dataValues.tags = [];
			});
			QuestTag.findAll({ attributes: ['questId', 'tagId'] }).then(tags => {
				tags.forEach((tag) => {
					for (var i = 0; i < quests.length; i++) {
						if (quests[i].questId === tag.questId) {
							quests[i].dataValues.tags.push(tag.tagId);
						}
					}
				});
				reply(quests);
			}).catch(err => {
				reply(boom.badImplementation(responses.get.responses['500'].description), err);
			})
		}).catch(err => {
			reply(boom.badImplementation(responses.get.responses['500'].description), err);
		});
	},
	getQuestsByUserId: (req, reply) => {
		Quest.findAll({
			attributes: ['questId', 'giverId', 'receiverId', 'name'],
			where: {
				cityId: req.auth.credentials.cityId,
				$or: [
					{ receiverId: req.params.userId },
					{ giverId: req.params.userId }
				]
			}
		}).then(quests => {
			quests.forEach(quest => {
				quest.dataValues.tags = [];
			});
			QuestTag.findAll({ attributes: ['questId', 'tagId'] }).then(tags => {
				tags.forEach((tag) => {
					quests.forEach(quest => {
						if (quest.questId === tag.questId) {
							quest.dataValues.tags.push(tag.tagId);
						}
					});
				});
				reply(quests);
			}).catch(err => {
				reply(boom.badImplementation(responses.get.responses['500'].description), err);
			})
		}).catch(err => {
			reply(boom.badImplementation(responses.get.responses['500'].description), err);
		});
	},
	getQuest: (req, reply) => {
		Quest.findOne({
			where: { questId: req.params.questId }
		}).then(quest => {
			// Get Applicants
			QuestUser.findAll({ where: { questId: quest.questId }}).then(questUsers => {
				quest.dataValues.applicants = [];
				questUsers.forEach(qU => {
					quest.dataValues.applicants.push(qU.userId);
				});
				// Get reward item
				if (quest.rewardItemId) {
					Item.findOne({
						where: { itemId: quest.rewardItemId }
					}).then(item => {
						if (item) {
							quest.dataValues.rewardCondition = item.condition;
							quest.dataValues.rewardName = item.name;
							quest.dataValues.rewardItemId = item.itemId;
							quest.dataValues.rewardDateAdded = item.dateAdded;
						}
						reply(quest);
					}).catch(err => {
						reply(boom.badImplementation(responses.getQuest.responses['500'].description), err);
					});
				} else {
					reply(quest);
				}
			});
		}).catch(err => {
			reply(boom.badImplementation(responses.getQuest.responses['500'].description), err);
		});
	},
	getQuestsByTag: (req, reply) => {
		QuestTag.findAll({
			attributes: ['questId'],
			where: { tagId: req.params.tagId }
		}).then(quests => {
			reply(quests);
		}).catch(err => {
			reply(boom.badImplementation(responses.getByTag.responses['500'].description), err);
		});
	},
	deleteQuest: (req, reply) => {
		Quest.destroy({
			where: {
				questId: req.params.questId
			}
		}).then(rows => {
			QuestUser.destroy({
				where: { questId: req.params.questId }
			}).then(rows => {
				QuestTag.destroy({
					where: {
						questId: req.params.questId
					}
				}).then(r => {
					reply().code(204);
				}).catch(err => {
					reply(boom.badImplementation(responses.tagQuest.responses['500'].description, err.name));
				});
			}).catch(err => {
				reply(boom.badImplementation(responses.delete.responses['500'].description, err.name));
			});
		}).catch(err => {
			reply(boom.badImplementation(responses.delete.responses['500'].description, err.name));
		});
	},
	tagQuest: (req, reply) => {
		QuestTag.create({
			tagId: req.params.tagId,
			questId: req.params.questId
		}, { fields: ['tagId', 'questId']}).then(r => {
			reply().code(204);
		}).catch(err => {
			reply(boom.badImplementation(responses.tagQuest.responses['500'].description, err.name));
		})
	},
	deleteTagQuest: (req, reply) => {
		QuestTag.destroy({
			where: {
				tagId: req.params.tagId,
				questId: req.params.questId
			}
		}).then(r => {
			reply().code(204);
		}).catch(err => {
			reply(boom.badImplementation(responses.tagQuest.responses['500'].description, err.name));
		})
	}
};

module.exports = quest;
