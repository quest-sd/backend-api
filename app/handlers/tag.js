const boom = require('boom');
const uuid = require('node-uuid');
const Tag = require('../models').Tag;
const responses = require('../../config/swagger').responses.tag;

const tag = {
	createTag: (req, reply) => {
		Tag.create({
			tagId: uuid.v4(),
			tagName: req.payload.name
		}).then(tag => {
			reply(tag).code(201);
		}).catch(err => {
			if (err.name === 'SequelizeUniqueConstraintError') {
				reply(boom.conflict(responses.post.responses['409'].description));
			} else { // Unhandled errors
				reply(boom.badImplementation(responses.post.responses['500'].description, err.name));
			}
		})
	},
	getTags: (req, reply) => {
		Tag.findAll().then(tags => {
			reply(tags);
		}).catch(err => {
			reply(boom.badImplementation(responses.get.responses['500'].description, err.name));
		})
	}
};

module.exports = tag;
