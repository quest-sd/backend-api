const boom = require('boom');
const Image = require('../models').Image;
const responses = require('../../config/swagger').responses.image;
const uuid = require('node-uuid');
const fs = require('fs');
const config = require('../../config/quest');

const image = {
    uploadImage: (req, reply) => {
        var data = req.payload;
        if (data.file) {
            const id = uuid.v4();
            const name = data.file.hapi.filename;
            const filename = id + '-' + name;
            const path = config.uploadDir + "/" + filename;
            const file = fs.createWriteStream(path);

            data.file.pipe(file);

            data.file.on('end', err => {
                const ret = {
                    filename: data.file.hapi.filename,
                    headers: data.file.hapi.headers
                };
                Image.create({
                    id: uuid.v4(),
                    filename: filename
                }).then(image => {
                    reply(image).code(201);
                }).catch(err => {
                    reply(boom.badImplementation(responses.post.responses['500'].description), err);
                });
            });
        }
    },
    getImage: (req, reply) => {
        Image.findOne({
            where: { id: req.params.imageId }
        }).then(image => {
            reply(image);
        }).catch(err => {
            reply(boom.badImplementation(responses.get.responses['500'].description), err);
        });
    }
};

module.exports = image;
