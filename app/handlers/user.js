const boom = require('boom');
const uuid = require('node-uuid');
const User = require('../models').User;
const ursa = require('ursa');
const fs = require('fs');
const config = require('../../config/quest');
const responses = require('../../config/swagger').responses.user;
const mail = require('nodemailer');
const hash = require('object-hash');

let transporter = mail.createTransport({
	service: 'gmail',
	auth: {
		user: 'questsd2017@gmail.com',
		pass: 'Quest1234!'
	}
});

const user = {
	resetPassword: (req, reply) => {
		const hashid = hash(req.payload.email);
		if (hashid !== req.payload.tempPassword) {
			reply(boom.badImplementation(responses.get.responses['500'].description));
		}
		User.update({
			password: req.payload.newPassword
		}, {
			where: { email: req.payload.email }
		}).then(result => {
			if (result[0] === 1) {
				reply().code(204);
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.put.responses['500'].description, err.name));
		});
	},
	sendForgotEmailPassword: (req, reply) => {
		User.findOne({
			where: { email: req.payload.email }
		}).then(user => {
			const hashid = hash(user.email);
			let mailOptions = {
				from: '"Quest Support" <questsd2017@gmail.com>',
				to: user.email,
				subject: 'Forgot Password',
				html: `<p>Below is your temporary password</p><p>${hashid}</p>`
			};
			transporter.sendMail(mailOptions, (err, info) => {
				if (err) {
					reply(boom.badImplementation(responses.get.responses['500'].description), err);
				}
				reply().code(204);
			});
		}).catch(err => {
			console.log(err);
			reply(boom.badImplementation(responses.get.responses['500'].description), err);
		});
	},
	createUser: (req, reply) => {
		const pass = req.payload.password;
		if (!pass && !req.payload.key) {
			reply(boom.unauthorized(responses.post.responses['401'].description));
		} else if (req.payload.key) {
			const pub = ursa.createPublicKey(req.payload.key, 'base64');
			const pri = ursa.createPrivateKey(fs.readFileSync(config.key));
			if (!ursa.matchingPublicKeys(pub, pri)) {
				reply(boom.unauthorized(responses.post.responses['401'].description));
			} else {
				User.create({
					userId: uuid.v4(),
					name: req.payload.name,
					email: req.payload.email,
					level: config.defaults.level,
					exp: config.defaults.exp,
					lastLogin: new Date(),
					joined: new Date(),
					cityId: req.payload.cityId,
					roleId: req.payload.roleId,
					profileImageId: req.payload.profileImageId
				}).then(user => {
					// Hide password
					user.password = undefined;
					reply(user).code(201);
				}).catch(err => {
					if (err.name === 'SequelizeUniqueConstraintError') {
						reply(boom.conflict(responses.post.responses['409'].description));
					} else { // Unhandled errors
						reply(boom.badImplementation(responses.post.responses['500'].description, err.name));
					}
				});
			}
		} else {
			User.create({
				userId: uuid.v4(),
				name: req.payload.name,
				email: req.payload.email,
				password: req.payload.password,
				level: config.defaults.level,
				exp: config.defaults.exp,
				lastLogin: new Date(),
				joined: new Date(),
				cityId: req.payload.cityId,
				roleId: req.payload.roleId,
				profileImageId: req.payload.profileImageId
			}).then(user => {
				// Hide password
				user.password = undefined;
				reply(user).code(201);
			}).catch(err => {
				if (err.name === 'SequelizeUniqueConstraintError') {
					reply(boom.conflict(responses.post.responses['409'].description));
				} else { // Unhandled errors
					reply(boom.badImplementation(responses.post.responses['500'].description, err.name));
				}
			});
		}
	},
	deleteUser: (req, reply) => {
		User.destroy({
			where: {
				userId: req.auth.credentials.userId
			}
		}).then(rows => {
			if (rows === 1) {
				reply().code(204);
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.delete.responses['500'].description, err.name));
		});
	},
	updateUserHealth: (req, reply) => {
		let update;
		if (req.payload.health === 12) {
			update = {
				health: req.payload.health,
				disabled: 1
			};
		} else {
			update = {
				health: req.payload.health,
				disabled: 0
			};
		}
		User.update(update, {
			where: { userId: req.params.id }
		}).then(result => {
			if (result[0] === 1) {
				reply().code(204);
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.put.responses['500'].description, err.name));
		});
	},
	updateUser: (req, reply) => {
		User.update({
			name: req.payload.name,
			email: req.payload.email,
			cityId: req.payload.cityId,
			roleId: req.payload.roleId,
			profileImageId: req.payload.profileImageId
		}, {
			where: { userId: req.auth.credentials.userId }
		}).then(result => {
			if (result[0] === 1) {
				reply().code(204);
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.put.responses['500'].description, err.name));
		});
	},
	getUser: (req, reply) => {
		User.findOne({
			where: { userId: req.params.id }
		}).then(user => {
			user.password = undefined;
			user.dataValues.expToNextLevel = Math.pow(((user.level + 1) / config.expConstant), 2);
			reply(user);
		}).catch(err => {
			reply(boom.badImplementation(responses.get.responses['500'].description), err);
		});
	},
	getUsers: (req, reply) => {
		User.findAll({
			attributes: ['userId', 'name', 'level', 'profileImageId', 'health'],
			where: { cityId: req.auth.credentials.cityId }
		}).then(users => {
			reply(users);
		}).catch(err => {
			reply(boom.badImplementation(responses.get.responses['500'].description), err);
		});
	}
};

module.exports = user;
