const boom = require('boom');
const uuid = require('node-uuid');
const User = require('../models').User;
const config = require('../../config/quest');
const responses = require('../../config/swagger').responses.login;
const jwtBuilder = require('../helpers/jwt');
const ursa = require('ursa');
const fs = require('fs');

const login = {
	standard: (req, reply) => {
		User.findOne({
			where: {
				email: req.payload.email,
				password: req.payload.password
			}
		}).then(user => {
			if (user && user.disabled !== 1) {
				const jwt = {
					jwt: jwtBuilder.login(user),
					userId: user.userId
				};
				reply(jwt);
			} else {
				reply(boom.unauthorized(responses.post.responses['401'].description));
			}
		}).catch(err => {
			reply(boom.badImplementation(responses.post.responses['500'].description, err.name));
		});
	},
	email: (req, reply) => {
		const pub = ursa.createPublicKey(req.payload.key, 'base64');
		const pri = ursa.createPrivateKey(fs.readFileSync(config.key));
		if (!ursa.matchingPublicKeys(pub, pri)) {
			reply(boom.unauthorized(responses.post.responses['401'].description));
		} else {
			User.findOne({
				where: {
					email: req.payload.email,
				}
			}).then(user => {
				if (user) {
					const jwt = {
						jwt: jwtBuilder.login(user),
						userId: user.userId
					};
					reply(jwt);
				} else {
					reply(boom.unauthorized(responses.post.responses['401'].description));
				}
			}).catch(err => {
				reply(boom.badImplementation(responses.post.responses['500'].description, err.name));
			});
		}
	}
};

module.exports = login;
