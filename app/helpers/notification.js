const config = require('../../config/quest');

module.exports = {
	sendNotification: (title, msg, email) => {
		const message = {
			app_id: config.appIdOneSignal,
			contents: { "en": msg },
			headings: { "en": title },
			filters: [
				{ "field": "tag", "key": "email", "value": email, "relation": "=" }
			]
		};

		const headers = {
			"Content-Type": "application/json; charset=utf-8",
			"Authorization": "Basic " + config.keyOneSignal
		};

		const options = {
			host: "onesignal.com",
			port: 443,
			path: "/api/v1/notifications",
			method: "POST",
			headers: headers
		};

		const https = require('https');
		const req = https.request(options, function(res) {
			res.on('data', function(data) {
				console.log("Response:");
				console.log(JSON.parse(data));
			});
		});

		req.on('error', function(e) {
			console.log("ERROR:");
			console.log(e);
		});

		req.write(JSON.stringify(message));
		req.end();
	}
};
