const jwt = require('jsonwebtoken');
const fs = require('fs');
const config = require('../../config/quest');
const key = fs.readFileSync(config.key);

const builder = {
	login: (user) => {
		const token = {
			userId: user.userId,
			email: user.email,
			cityId: user.cityId
		};
		return jwt.sign(token, key);
	}
};

module.exports = builder;
