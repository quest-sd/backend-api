module.exports = (sequelize, DataTypes) => {
    var Role = sequelize.define('Role', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            field: 'role_id'
        },
        name: {
            type: DataTypes.STRING,
            field: 'role_name'
        }
    }, {
        tableName: 'ROLES',
        updatedAt: false,
        createdAt: false
    });
    return Role;
};
