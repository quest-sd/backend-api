module.exports = (sequelize, DataTypes) => {
	var UserReport = sequelize.define('UserReport', {
		reportId: {
			type: DataTypes.UUID,
			primaryKey: true,
			field: 'report_id'
		},
		questId: {
			type: DataTypes.UUID,
			field: 'quest_id'
		},
		reporterId: {
			type: DataTypes.UUID,
			field: 'reporter_id'
		},
		offenderId: {
			type: DataTypes.UUID,
			field: 'offender_id'
		},
		timeStamp: {
			type: DataTypes.DATE,
			field: 'timestamp'
		},
		status: DataTypes.STRING,
		reason: DataTypes.STRING,
		message: DataTypes.STRING
	}, {
		tableName: 'USERS_REPORT',
		updatedAt: false,
		createdAt: false
	});
	return UserReport;
};
