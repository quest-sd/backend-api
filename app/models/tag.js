module.exports = (sequelize, DataTypes) => {
	var Tag = sequelize.define('Tag', {
		tagId: {
			type: DataTypes.UUID,
			primaryKey: true,
			field: 'tag_id'
		},
		tagName: {
			type: DataTypes.STRING,
			field: 'tag_name'
		}
	}, {
		tableName: 'TAGS',
		updatedAt: false,
		createdAt: false
	});
	return Tag;
};
