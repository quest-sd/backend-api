module.exports = (sequelize, DataTypes) => {
	var Quest = sequelize.define('Quest', {
		questId: {
			type: DataTypes.UUID,
			primaryKey: true,
			field: 'quest_id'
		},
		receiverId: {
			type: DataTypes.UUID,
			field: 'receiver_id'
		},
		giverId: {
			type: DataTypes.UUID,
			field: 'giver_id'
		},
		cityId: {
			type: DataTypes.UUID,
			field: 'city_id'
		},
		rewardItemId: {
			type: DataTypes.UUID,
			field: 'reward_item_id'
		},
		name: DataTypes.STRING,
		rewardExp: {
			type: DataTypes.INTEGER,
			field: 'reward_exp'
		},
		created: DataTypes.DATE,
		completedDate: {
			type: DataTypes.DATE,
			field: 'completed_date'
		},
		status: DataTypes.STRING,
		description: DataTypes.STRING,
		address: DataTypes.STRING,
		latitude: DataTypes.DECIMAL,
		longitude: DataTypes.DECIMAL,
		cashReward: {
			type: DataTypes.DECIMAL,
			field: 'cash_reward'
		},
		giverRating: {
			type: DataTypes.INTEGER,
			field: 'giver_rating'
		},
		giverComment: {
			type: DataTypes.STRING,
			field: 'giver_comment'
		},
		receiverRating: {
			type: DataTypes.INTEGER,
			field: 'receiver_rating'
		},
		receiverComment: {
			type: DataTypes.STRING,
			field: 'receiver_comment'
		}
	}, {
		tableName: 'QUESTS',
		updatedAt: false,
		createdAt: false
	});
	return Quest;
};
