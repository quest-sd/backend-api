module.exports = (sequelize, DataTypes) => {
	var QuestTag = sequelize.define('QuestTag', {
		tagId: {
			type: DataTypes.UUID,
			field: 'tag_id'
		},
		questId: {
			type: DataTypes.UUID,
			field: 'quest_id'
		}
	}, {
		tableName: 'QUESTS_TAGS',
		updatedAt: false,
		createdAt: false
	});
	return QuestTag;
};
