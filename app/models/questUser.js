module.exports = (sequelize, DataTypes) => {
	var QuestUser = sequelize.define('QuestUser', {
		userId: {
			type: DataTypes.UUID,
			field: 'user_id'
		},
		questId: {
			type: DataTypes.UUID,
			field: 'quest_id'
		}
	}, {
		tableName: 'QUESTS_USERS',
		updatedAt: false,
		createdAt: false
	});
	QuestUser.removeAttribute('id');
	return QuestUser;
};
