module.exports = (sequelize, DataTypes) => {
	var City = sequelize.define('City', {
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			field: 'city_id'
		},
		name: {
			type: DataTypes.STRING,
			field: 'city_name'
		}
	}, {
		tableName: 'CITIES',
		updatedAt: false,
		createdAt: false
	});
	return City;
};
