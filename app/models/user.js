module.exports = (sequelize, DataTypes) => {
	var User = sequelize.define('User', {
		userId: {
			type: DataTypes.UUID,
			primaryKey: true,
			field: 'user_id'
		},
		name: DataTypes.STRING,
		email: DataTypes.STRING,
		password: DataTypes.STRING,
		level: DataTypes.INTEGER,
		exp: DataTypes.INTEGER,
		lastLogin: {
			type: DataTypes.DATE,
			field: 'last_login'
		},
		joined: DataTypes.DATE,
		cityId: {
			type: DataTypes.UUID,
			field: 'city_id'
		},
		roleId: {
			type: DataTypes.UUID,
			field: 'role_id'
		},
		profileImageId: {
			type: DataTypes.UUID,
			field: 'profile_image_id'
		},
		health: DataTypes.INTEGER,
		disabled: DataTypes.INTEGER
	}, {
		tableName: 'USERS',
		updatedAt: false,
		createdAt: false
	});
	return User;
};
