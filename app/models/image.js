module.exports = (sequelize, DataTypes) => {
    var Image = sequelize.define('Image', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            field: 'image_id'
        },
        filename: {
            type: DataTypes.STRING,
            field: 'image_filename'
        }
    }, {
        tableName: 'IMAGES',
        updatedAt: false,
        createdAt: false
    });
    return Image;
};
