module.exports = (sequelize, DataTypes) => {
	var Item = sequelize.define('Item', {
		itemId: {
			type: DataTypes.UUID,
			primaryKey: true,
			field: 'item_id'
		},
		imageId: {
			type: DataTypes.UUID,
			field: 'image_id'
		},
		ownerId: {
			type: DataTypes.UUID,
			field: 'owner_id'
		},
		name: {
			type: DataTypes.STRING,
			field: 'item_name'
		},
        description: {
			type: DataTypes.STRING,
			field: 'description'
        },
        dateAdded: {
		    type: DataTypes.DATE,
			field: 'date_added'
        },
        condition: {
			type: DataTypes.STRING,
			field: 'condition'
        }
	}, {
		tableName: 'ITEMS',
		updatedAt: false,
		createdAt: false
	});
	return Item;
};
