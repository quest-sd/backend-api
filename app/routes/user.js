const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/user');

const user = [
	{
		method: 'POST',
		path: '/user',
		handler: handler.createUser,
		config: {
			auth: false,
			tags: swagger.routes.user.tags,
			description: swagger.routes.user.post.description,
			notes: swagger.routes.user.post.notes,
			plugins: {
				'hapi-swagger': swagger.responses.user.post
			},
			validate: {
				payload: swagger.schemas.user.post.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'PUT',
		path: '/user',
		handler: handler.updateUser,
		config: {
			tags: swagger.routes.user.tags,
			description: swagger.routes.user.put.description,
			notes: swagger.routes.user.put.notes,
			plugins: {
				'hapi-swagger': swagger.responses.user.put
			},
			validate: {
				headers: swagger.schemas.authorization,
				payload: swagger.schemas.user.put.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'PUT',
		path: '/user/{id}',
		handler: handler.updateUserHealth,
		config: {
			tags: swagger.routes.user.tags,
			description: swagger.routes.user.putById.description,
			notes: swagger.routes.user.putById.notes,
			plugins: {
				'hapi-swagger': swagger.responses.user.putById
			},
			validate: {
				headers: swagger.schemas.authorization,
				payload: swagger.schemas.user.putById.payload,
				params:  {
					id: swagger.schemas.user.putById.param
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'GET',
		path: '/user',
		handler: handler.getUsers,
		config: {
			tags: swagger.routes.user.tags,
			description: swagger.routes.user.get.description,
			notes: swagger.routes.user.get.notes,
			plugins: {
				'hapi-swagger': swagger.responses.user.get
			},
			validate: {
				headers: swagger.schemas.authorization
			}
		}
	},
	{
		method: 'GET',
		path: '/user/{id}',
		handler: handler.getUser,
		config: {
			tags: swagger.routes.user.tags,
			description: swagger.routes.user.getUser.description,
			notes: swagger.routes.user.getUser.notes,
			plugins: {
				'hapi-swagger': swagger.responses.user.getUser
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					id: swagger.schemas.user.getUser.param
				},
			}
		}
	},
	{
		method: 'DELETE',
		path: '/user',
		handler: handler.deleteUser,
		config: {
			tags: swagger.routes.user.tags,
			description: swagger.routes.user.delete.description,
			notes: swagger.routes.user.delete.notes,
			plugins: {
				'hapi-swagger': swagger.responses.user.delete
			},
			validate: {
				headers: swagger.schemas.authorization
			}
		}
	},
	{
		method: 'POST',
		path: '/user/forgot',
		handler: handler.sendForgotEmailPassword,
		config: {
			auth: false,
			tags: swagger.routes.user.tags,
			description: swagger.routes.user.forgotPassword.description,
			notes: swagger.routes.user.forgotPassword.notes,
			plugins: {
				'hapi-swagger': swagger.responses.user.forgotPassword
			},
			validate: {
				payload: swagger.schemas.user.forgotPassword.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'POST',
		path: '/user/password',
		handler: handler.resetPassword,
		config: {
			auth: false,
			tags: swagger.routes.user.tags,
			description: swagger.routes.user.resetPassword.description,
			notes: swagger.routes.user.resetPassword.notes,
			plugins: {
				'hapi-swagger': swagger.responses.user.resetPassword
			},
			validate: {
				payload: swagger.schemas.user.resetPassword.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
];

module.exports = user;
