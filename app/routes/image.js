const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/image');
const config = require('../../config/quest');

const image = [
    {
        method: 'POST',
        path: '/image',
        handler: handler.uploadImage,
        config: {
            auth: false,
            tags: swagger.routes.image.tags,
            description: swagger.routes.image.post.description,
            notes: swagger.routes.image.post.notes,
            plugins: {
                'hapi-swagger': swagger.responses.image.post
            },
            payload: {
                output: 'stream',
                parse: true,
                allow: 'multipart/form-data'
            }
        }
    },
    {
        method: 'GET',
        path: '/image/{imageId}',
        handler: handler.getImage,
        config: {
            auth: false,
            tags: swagger.routes.image.tags,
            description: swagger.routes.image.get.description,
            notes: swagger.routes.image.get.notes,
            plugins: {
                'hapi-swagger': swagger.responses.image.get
            },
            validate: {
                params: {
                    imageId: swagger.schemas.image.get.param
                },
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/image/file/{filename}',
        handler: {
            directory: {
                path: config.uploadDir,
                listing: true
            }
        },
        config: {
            auth: false,
            tags: swagger.routes.image.tags,
            description: swagger.routes.image.getFile.description,
            notes: swagger.routes.image.getFile.notes,
            plugins: {
                'hapi-swagger': swagger.responses.image.getFile
            },
            validate: {
                params: {
                    filename: swagger.schemas.image.getFile.param
                },
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    }
];

module.exports = image;
