const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/city');


const city = [
    {
        method: 'POST',
        path: '/city',
        handler: handler.addCity,
        config: {
            tags: swagger.routes.city.tags,
            description: swagger.routes.city.post.description,
            notes: swagger.routes.city.post.notes,
            plugins: {
                'hapi-swagger': swagger.responses.city.post
            },
            validate: {
                headers: swagger.schemas.authorization,
                payload: swagger.schemas.city.post.payload,
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/city',
        handler: handler.updateCity,
        config: {
            tags: swagger.routes.city.tags,
            description: swagger.routes.city.put.description,
            notes: swagger.routes.city.put.notes,
            plugins: {
                'hapi-swagger': swagger.responses.city.put
            },
            validate: {
                headers: swagger.schemas.authorization,
                payload: swagger.schemas.city.put.payload,
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/city',
        handler: handler.getCities,
        config: {
            auth: false,
            tags: swagger.routes.city.tags,
            description: swagger.routes.city.get.description,
            notes: swagger.routes.city.get.notes,
            plugins: {
                'hapi-swagger': swagger.responses.city.get
            }
        }
    }
];

module.exports = city;
