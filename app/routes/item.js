const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/item');
const config = require('../../config/quest');

const item = [
	{
		method: 'POST',
		path: '/item',
		handler: handler.addItem,
		config: {
			tags: swagger.routes.item.tags,
			description: swagger.routes.item.post.description,
			notes: swagger.routes.item.post.notes,
			plugins: {
				'hapi-swagger': swagger.responses.item.post
			},
			validate: {
				headers: swagger.schemas.authorization,
				payload: swagger.schemas.item.post.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'GET',
		path: '/item',
		handler: handler.getItems,
		config: {
			tags: swagger.routes.item.tags,
			description: swagger.routes.item.get.description,
			notes: swagger.routes.item.get.notes,
			plugins: {
				'hapi-swagger': swagger.responses.item.get
			},
			validate: {
				headers: swagger.schemas.authorization,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'GET',
		path: '/item/{itemId}',
		handler: handler.getItem,
		config: {
			tags: swagger.routes.item.tags,
			description: swagger.routes.item.getItem.description,
			notes: swagger.routes.item.getItem.notes,
			plugins: {
				'hapi-swagger': swagger.responses.item.getItem
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					itemId: swagger.schemas.item.getItem.param
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'PUT',
		path: '/item',
		handler: handler.updateItem,
		config: {
			tags: swagger.routes.item.tags,
			description: swagger.routes.item.put.description,
			notes: swagger.routes.item.put.notes,
			plugins: {
				'hapi-swagger': swagger.responses.item.put
			},
			validate: {
				headers: swagger.schemas.authorization,
				payload: swagger.schemas.item.put.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'DELETE',
		path: '/item/{id}',
		handler: handler.deleteItem,
		config: {
			tags: swagger.routes.item.tags,
			description: swagger.routes.item.delete.description,
			notes: swagger.routes.item.delete.notes,
			plugins: {
				'hapi-swagger': swagger.responses.item.delete
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					id: swagger.schemas.item.delete.param
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	}
];

module.exports = item;
