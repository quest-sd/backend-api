const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/tag');

const tag = [
	{
		method: 'POST',
		path: '/tag',
		handler: handler.createTag,
		config: {
			tags: swagger.routes.tag.tags,
			description: swagger.routes.tag.post.description,
			notes: swagger.routes.tag.post.notes,
			plugins: {
				'hapi-swagger': swagger.responses.tag.post
			},
			validate: {
				headers: swagger.schemas.authorization,
				payload: swagger.schemas.tag.post.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'GET',
		path: '/tag',
		handler: handler.getTags,
		config: {
			tags: swagger.routes.tag.tags,
			description: swagger.routes.tag.get.description,
			notes: swagger.routes.tag.get.notes,
			plugins: {
				'hapi-swagger': swagger.responses.tag.get
			},
			validate: {
				headers: swagger.schemas.authorization
			}
		}
	}
];

module.exports = tag;
