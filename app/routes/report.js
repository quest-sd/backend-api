const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/report');

const report = [
	{
		method: 'POST',
		path: '/report',
		handler: handler.createReport,
		config: {
			tags: swagger.routes.report.tags,
			description: swagger.routes.report.post.description,
			notes: swagger.routes.report.post.notes,
			plugins: {
				'hapi-swagger': swagger.responses.report.post
			},
			validate: {
				headers: swagger.schemas.authorization,
				payload: swagger.schemas.report.post.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'GET',
		path: '/report',
		handler: handler.getReports,
		config: {
			tags: swagger.routes.report.tags,
			description: swagger.routes.report.get.description,
			notes: swagger.routes.report.get.notes,
			plugins: {
				'hapi-swagger': swagger.responses.report.get
			},
			validate: {
				headers: swagger.schemas.authorization,
			}
		}
	},
	{
		method: 'PUT',
		path: '/report/{id}',
		handler: handler.resolveReport,
		config: {
			tags: swagger.routes.report.tags,
			description: swagger.routes.report.put.description,
			notes: swagger.routes.report.put.notes,
			plugins: {
				'hapi-swagger': swagger.responses.report.put
			},
			validate: {
				headers: swagger.schemas.authorization,
				params: {
					id: swagger.schemas.user.putById.param
				},
				payload: swagger.schemas.report.put.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	}
];

module.exports = report;
