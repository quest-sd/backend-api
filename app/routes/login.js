const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/login');

const login = [
    {
        method: 'POST',
        path: '/login',
        handler: handler.standard,
        config: {
            auth: false,
            tags: swagger.routes.login.tags,
            description: swagger.routes.login.post.description,
            notes: swagger.routes.login.post.notes,
            plugins: {
                'hapi-swagger': swagger.responses.login.post
            },
            validate: {
                payload: swagger.schemas.login.post.payload,
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    },
	{
		method: 'POST',
		path: '/login/email',
		handler: handler.email,
		config: {
			auth: false,
			tags: swagger.routes.login.tags,
			description: swagger.routes.login.email.description,
			notes: swagger.routes.login.email.notes,
			plugins: {
				'hapi-swagger': swagger.responses.login.email
			},
			validate: {
				payload: swagger.schemas.login.email.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	}
];

module.exports = login;
