const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/quest');

const quest = [
    {
        method: 'POST',
        path: '/quest',
        handler: handler.createQuest,
        config: {
            tags: swagger.routes.quest.tags,
            description: swagger.routes.quest.post.description,
            notes: swagger.routes.quest.post.notes,
            plugins: {
                'hapi-swagger': swagger.responses.quest.post
            },
            validate: {
                headers: swagger.schemas.authorization,
                payload: swagger.schemas.quest.post.payload,
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    },
	{
		method: 'PUT',
		path: '/quest/{questId}/apply/{userId}',
		handler: handler.applyForQuest,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.apply.description,
			notes: swagger.routes.quest.apply.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.apply
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					questId: swagger.schemas.quest.apply.param,
					userId: swagger.schemas.quest.apply.param,
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'PUT',
		path: '/quest/{questId}/accept/{userId}',
		handler: handler.acceptForQuest,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.accept.description,
			notes: swagger.routes.quest.accept.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.accept
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					questId: swagger.schemas.quest.accept.param,
					userId: swagger.schemas.quest.accept.param,
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'PUT',
		path: '/quest/{questId}/complete',
		handler: handler.completeQuest,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.complete.description,
			notes: swagger.routes.quest.complete.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.complete
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					questId: swagger.schemas.quest.apply.param,
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'PUT',
		path: '/quest/{questId}/abandon',
		handler: handler.abandonQuest,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.abandon.description,
			notes: swagger.routes.quest.abandon.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.abandon
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					questId: swagger.schemas.quest.apply.param,
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'PUT',
		path: '/quest/{questId}/rate',
		handler: handler.rateQuest,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.rate.description,
			notes: swagger.routes.quest.rate.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.rate
			},
			validate: {
				headers: swagger.schemas.authorization,
				payload: swagger.schemas.quest.rate.payload,
				params:  {
					questId: swagger.schemas.quest.accept.param,
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
    {
        method: 'PUT',
        path: '/quest/{questId}',
        handler: handler.updateQuest,
        config: {
            tags: swagger.routes.quest.tags,
            description: swagger.routes.quest.put.description,
            notes: swagger.routes.quest.put.notes,
            plugins: {
                'hapi-swagger': swagger.responses.quest.put
            },
            validate: {
                headers: swagger.schemas.authorization,
                payload: swagger.schemas.quest.put.payload,
	            params:  {
		            questId: swagger.schemas.quest.put.param
	            },
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/quest',
        handler: handler.getQuests,
        config: {
            tags: swagger.routes.quest.tags,
            description: swagger.routes.quest.get.description,
            notes: swagger.routes.quest.get.notes,
            plugins: {
                'hapi-swagger': swagger.responses.quest.get
            },
            validate: {
                headers: swagger.schemas.authorization,
            }
        }
    },
    {
        method: 'GET',
        path: '/quest/{questId}',
        handler: handler.getQuest,
        config: {
            tags: swagger.routes.quest.tags,
            description: swagger.routes.quest.getQuest.description,
            notes: swagger.routes.quest.getQuest.notes,
            plugins: {
                'hapi-swagger': swagger.responses.quest.getQuest
            },
            validate: {
                headers: swagger.schemas.authorization,
                params:  {
                    questId: swagger.schemas.quest.getQuest.param
                },
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    },
	{
		method: 'GET',
		path: '/quest/tag/{tagId}',
		handler: handler.getQuestsByTag,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.getByTag.description,
			notes: swagger.routes.quest.getByTag.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.getByTag
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					tagId: swagger.schemas.quest.getByTag.param
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'GET',
		path: '/quest/user/{userId}',
		handler: handler.getQuestsByUserId,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.getByUserId.description,
			notes: swagger.routes.quest.getByUserId.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.getByUserId
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					userId: swagger.schemas.quest.getByUserId.param
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'PUT',
		path: '/quest/{questId}/tag/{tagId}',
		handler: handler.tagQuest,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.tagQuest.description,
			notes: swagger.routes.quest.tagQuest.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.tagQuest
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					questId: swagger.schemas.quest.tagQuest.quest,
					tagId: swagger.schemas.quest.tagQuest.tag
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'DELETE',
		path: '/quest/{questId}/tag/{tagId}',
		handler: handler.deleteTagQuest,
		config: {
			tags: swagger.routes.quest.tags,
			description: swagger.routes.quest.deleteTagQuest.description,
			notes: swagger.routes.quest.tagQuest.notes,
			plugins: {
				'hapi-swagger': swagger.responses.quest.deleteTagQuest
			},
			validate: {
				headers: swagger.schemas.authorization,
				params:  {
					questId: swagger.schemas.quest.deleteTagQuest.quest,
					tagId: swagger.schemas.quest.deleteTagQuest.tag
				},
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
    {
        method: 'DELETE',
        path: '/quest/{questId}',
        handler: handler.deleteQuest,
        config: {
            tags: swagger.routes.quest.tags,
            description: swagger.routes.quest.delete.description,
            notes: swagger.routes.quest.delete.notes,
            plugins: {
                'hapi-swagger': swagger.responses.quest.delete
            },
            validate: {
                headers: swagger.schemas.authorization,
                params:  {
                    questId: swagger.schemas.quest.delete.param
                },
                failAction: (req, reply, src, err) => {
                    reply(boom.badData(err.data));
                }
            }
        }
    }
];

module.exports = quest;
