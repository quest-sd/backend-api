const boom = require('boom');
const swagger = require('../../config/swagger');
const handler = require('../handlers/role');

const role = [
	{
		method: 'POST',
		path: '/role',
		handler: handler.createRole,
		config: {
			tags: swagger.routes.role.tags,
			description: swagger.routes.role.post.description,
			notes: swagger.routes.role.post.notes,
			plugins: {
				'hapi-swagger': swagger.responses.role.post
			},
			validate: {
				payload: swagger.schemas.role.post.payload,
				failAction: (req, reply, src, err) => {
					reply(boom.badData(err.data));
				}
			}
		}
	},
	{
		method: 'GET',
		path: '/role',
		handler: handler.getRoles,
		config: {
			auth: false,
			tags: swagger.routes.role.tags,
			description: swagger.routes.role.get.description,
			notes: swagger.routes.role.get.notes,
			plugins: {
				'hapi-swagger': swagger.responses.role.get
			}
		}
	}
];

module.exports = role;
