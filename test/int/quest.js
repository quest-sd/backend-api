const expect = require('chai').expect;
const server = require('../../index');
const fixtures = require('../fixtures');

let status;

describe('When POST /quest is requested', () => {

	let jwt, userId, quest, cities;
	before(done => {
		server.inject({
			method: 'GET',
			url: '/city'
		}, response => {
			cities = JSON.parse(response.payload);
			server.inject({
				method: 'POST',
				url: '/user',
				payload: {
					name: fixtures.name,
					email: fixtures.email,
					password: fixtures.password,
					roleId: fixtures.roleId,
					profileImageId: fixtures.profileImageId,
					cityId: cities[0].id
				}
			}, response => {
				createdUser = JSON.parse(response.payload);
				server.inject({
					method: 'POST',
					url: '/login',
					payload: {
						email: fixtures.email,
						password: fixtures.password
					}
				}, response => {
					jwt = JSON.parse(response.payload).jwt;
					userId = JSON.parse(response.payload).userId;
					server.inject({
						method: 'POST',
						url: '/quest',
						payload: {
							name: fixtures.questName,
							description: fixtures.questDescription,
							address: fixtures.questAddress,
							latitude: fixtures.questLatitude,
							longitude: fixtures.questLongitude,
							cashReward: fixtures.questCashReward
						},
						headers: {
							authorization: `Bearer ${jwt}`
						}
					}, response => {
						quest = JSON.parse(response.payload);
						status = response.statusCode;
						done();
					});
				});
			});
		});
	});

	it('should return a 201', () => {
		expect(status).to.equal(201);
	});

	describe('When GET /tags is requested', () => {

		let tags;
		before(done => {
			server.inject({
				method: 'GET',
				url: '/tag',
				headers: {
					authorization: 'Bearer ' + jwt
				}
			}, response => {
				tags = JSON.parse(response.payload);
				status = response.statusCode;
				done();
			});
		});

		it('should return a 200', () => {
			expect(status).to.equal(200);
		});

		it('should return a list of tags', () => {
			expect(tags).to.be.instanceOf(Array);
		});

		describe('When PUT /quest/{qId}/tag/{tId} is requested', () => {
			before(done => {
				server.inject({
					method: 'PUT',
					url: `/quest/${quest.questId}/tag/${tags[0].tagId}`,
					headers: {
						authorization: 'Bearer ' + jwt
					}
				}, response => {
					status = response.statusCode;
					done();
				});
			});

			it('should return a 204', () => {
				expect(status).to.equal(204);
			});

			describe('When GET /quest is requested', () => {

				let questsWithTags;
				before(done => {
					server.inject({
						method: 'GET',
						url: `/quest`,
						headers: {
							authorization: 'Bearer ' + jwt
						}
					}, response => {
						status = response.statusCode;
						questsWithTags = JSON.parse(response.payload);
						done();
					});
				});

				it('should return a 200', () => {
					expect(status).to.equal(200);
				});

				it('should return a list of quests with tags and names', () => {
					expect(questsWithTags[0].name).to.be.ok;
					var questWithTag = questsWithTags.filter((q) => {
						return q.tags.length != 0
					})[0];
					expect(questWithTag.tags).to.be.ok;
				});
			});

			describe('When GET /quest/tag/{tId} is requested', () => {

				let tagQuests;
				before(done => {
					server.inject({
						method: 'GET',
						url: `/quest/tag/${tags[0].tagId}`,
						headers: {
							authorization: 'Bearer ' + jwt
						}
					}, response => {
						status = response.statusCode;
						tagQuests = JSON.parse(response.payload);
						done();
					});
				});

				it('should return a 200', () => {
					expect(status).to.equal(200);
				});

				it('should return a list of quests', () => {
					expect(tags).to.be.instanceOf(Array);
					expect(tags[0]).to.not.be.empty;
				});

				describe('When DELETE /quest/{qId}/tag/{tId} is requested', () => {
					before(done => {
						server.inject({
							method: 'DELETE',
							url: `/quest/${quest.questId}/tag/${tags[0].tagId}`,
							headers: {
								authorization: 'Bearer ' + jwt
							}
						}, response => {
							status = response.statusCode;
							done();
						});
					});

					it('should return a 204', () => {
						expect(status).to.equal(204);
					});
				});
			});
		});
	});

	describe('When GET /quest is requested', () => {

		let quests;
		before(done => {
			server.inject({
				method: 'GET',
				url: '/quest',
				headers: {
					authorization: 'Bearer ' + jwt
				}
			}, response => {
				quests = JSON.parse(response.payload);
				status = response.statusCode;
				done();
			});
		});

		it('should return a 200', () => {
			expect(status).to.equal(200);
		});

		it('should return list of quests', () => {
			expect(quests).to.be.instanceOf(Array);
		});

		describe('When PUT /quest/{questId}/apply/{userId} is requested by the quester to apply to a quest', () => {

			let otherJwt, otherUserId;
			before(done => {
				server.inject({
					method: 'POST',
					url: '/user',
					payload: {
						name: fixtures.name,
						email: fixtures.otherEmail,
						password: fixtures.password,
						roleId: fixtures.roleId,
						profileImageId: fixtures.profileImageId,
						cityId: cities[0].id
					}
				}, response => {
					server.inject({
						method: 'POST',
						url: '/login',
						payload: {
							email: fixtures.otherEmail,
							password: fixtures.password
						}
					}, response => {
						otherJwt = JSON.parse(response.payload).jwt;
						otherUserId = JSON.parse(response.payload).userId;
						server.inject({
							method: 'PUT',
							url: `/quest/${quest.questId}/apply/${otherUserId}`,
							headers: {
								authorization: `Bearer ${otherJwt}`
							}
						}, response => {
							status = response.statusCode;
							done();
						});
					});
				});
			});

			it('should return a 204', () => {
				expect(status).to.equal(204);
			});

			describe('When GET /quest/{id} is requested', () => {

				let quest0;
				before(done => {
					server.inject({
						method: 'GET',
						url: `/quest/${quest.questId}`,
						headers: {
							authorization: `Bearer  ${otherJwt}`
						}
					}, response => {
						quest0 = JSON.parse(response.payload);
						status = response.statusCode;
						done();
					});
				});

				it('should return a 200', () => {
					expect(status).to.equal(200);
				});

				it('should return a quest in OPEN status', () => {
					expect(quest0.status).to.equal('OPEN');
				});

				it('should return a quest with a cash reward', () => {
					expect(quest0.cashReward).to.equal(fixtures.questCashReward);
				});

				it('should return a quest with one applicant', () => {
					expect(quest0.applicants[0]).to.equal(otherUserId);
				});

				describe('When POST /item is requested to add an item', () => {

					let item;
					before(done => {
						server.inject({
							method: 'POST',
							url: `/item`,
							headers: {
								authorization: `Bearer  ${otherJwt}`
							},
							payload: {
								name: fixtures.itemName,
								description: fixtures.itemDescription,
								condition: fixtures.itemCondition
							}
						}, response => {
							item = JSON.parse(response.payload);
							status = response.statusCode;
							done();
						});
					});

					it('should return a 201', () => {
						expect(status).to.equal(201);
					});

					it('should return an item', () => {
						expect(item).to.be.ok;
					});

					describe('When GET /item is requested to get items', () => {

						let items;
						before(done => {
							server.inject({
								method: 'GET',
								url: `/item`,
								headers: {
									authorization: `Bearer  ${otherJwt}`
								}
							}, response => {
								items = JSON.parse(response.payload);
								status = response.statusCode;
								done();
							});
						});

						it('should return a 200', () => {
							expect(status).to.equal(200);
						});

						it('should return an array of items', () => {
							expect(items).to.be.instanceOf(Array);
						});

						describe('When PUT /item is requested to update an item', () => {

							let item;
							before(done => {
								server.inject({
									method: 'PUT',
									url: `/item`,
									headers: {
										authorization: `Bearer  ${otherJwt}`
									},
									payload: {
										itemId: items[0].itemId,
										description: 'New Description!',
									}
								}, response => {
									status = response.statusCode;
									done();
								});
							});

							it('should return a 204', () => {
								expect(status).to.equal(204);
							});

							describe('When GET /item/{itemId} is requested', () => {

								let item;
								before(done => {
									server.inject({
										method: 'GET',
										url: `/item/${items[0].itemId}`,
										headers: {
											authorization: `Bearer  ${otherJwt}`
										}
									}, response => {
										item = JSON.parse(response.payload);
										status = response.statusCode;
										done();
									});
								});

								it('should return a 200', () => {
									expect(status).to.equal(200);
								});

								it('should return an item', () => {
									expect(item.itemId).to.equal(items[0].itemId);
								});
							});

							describe('When PUT /quest/{id} is requested', () => {

								before(done => {
									server.inject({
										method: 'PUT',
										url: `/quest/${quest.questId}`,
										headers: {
											authorization: `Bearer  ${jwt}`
										},
										payload: {
											rewardItemId: items[0].itemId,
										}
									}, response => {
										status = response.statusCode;
										done();
									});
								});

								it('should return a 204', () => {
									expect(status).to.equal(204);
								});

								describe('When GET /quest/{id} is requested', () => {

									let questItem;
									before(done => {
										server.inject({
											method: 'GET',
											url: `/quest/${quest.questId}`,
											headers: {
												authorization: `Bearer  ${jwt}`
											}
										}, response => {
											questItem = JSON.parse(response.payload);
											status = response.statusCode;
											done();
										});
									});

									it('should return a 200', () => {
										expect(status).to.equal(200);
									});

									it('should return a quest with reward condition and reward name', () => {
										expect(questItem.rewardCondition).to.equal(fixtures.itemCondition);
										expect(questItem.rewardName).to.equal(fixtures.itemName);
										expect(questItem.rewardItemId).to.be.ok;
									});

									describe('When DELETE /item is requested to remove an item', () => {

										before(done => {
											server.inject({
												method: 'DELETE',
												url: `/item/${items[0].itemId}`,
												headers: {
													authorization: `Bearer  ${otherJwt}`
												}
											}, response => {
												status = response.statusCode;
												done();
											});
										});

										it('should return a 204', () => {
											expect(status).to.equal(204);
										});
									});
								});
							});
						});
					});
				});

				describe('When PUT /quest/{questId}/accept/{userId} is requested by the quest giver to accept the user to the quest', () => {

					before(done => {
						server.inject({
							method: 'PUT',
							url: `/quest/${quest.questId}/accept/${otherUserId}`,
							headers: {
								authorization: `Bearer  ${otherJwt}`
							}
						}, response => {
							status = response.statusCode;
							done();
						});
					});

					it('should return a 204', () => {
						expect(status).to.equal(204);
					});

					describe('When GET /quest/user/{id} is requested', () => {

						let otherUsersQuests;
						before(done => {
							server.inject({
								method: 'GET',
								url: `/quest/user/${otherUserId}`,
								headers: {
									authorization: 'Bearer ' + jwt
								}
							}, response => {
								otherUsersQuests = JSON.parse(response.payload);
								status = response.statusCode;
								done();
							});
						});

						it('should return a 200', () => {
							expect(status).to.equal(200);
						});

						it('should return a list of quests', () => {
							expect(otherUsersQuests).to.be.instanceOf(Array);
						});

						describe('When GET /quest/user/{id} is requested', () => {

							let userQuests;
							before(done => {
								server.inject({
									method: 'GET',
									url: `/quest/user/${userId}`,
									headers: {
										authorization: 'Bearer ' + jwt
									}
								}, response => {
									userQuests = JSON.parse(response.payload);
									status = response.statusCode;
									done();
								});
							});

							it('should return a 200', () => {
								expect(status).to.equal(200);
							});

							it('should return a list of quests', () => {
								expect(userQuests).to.be.instanceOf(Array);
							});

							it('should return an equal list of quests', () => {
								expect(userQuests.length).to.be.equal(otherUsersQuests.length);
								expect(userQuests[0].questId).to.be.equal(otherUsersQuests[0].questId);
							});
						});
					});

					describe('When GET /quest/{id} is requested', () => {

						let quest1;
						before(done => {
							server.inject({
								method: 'GET',
								url: `/quest/${quest.questId}`,
								headers: {
									authorization: `Bearer  ${jwt}`
								}
							}, response => {
								quest1 = JSON.parse(response.payload);
								status = response.statusCode;
								done();
							});
						});

						it('should return a 200', () => {
							expect(status).to.equal(200);
						});

						it('should return a quest in progress', () => {
							expect(quest1.status).to.equal('IN_PROGRESS');
						});

						it('should return a quest with a receiver ID', () => {
							expect(quest1.receiverId).to.equal(otherUserId);
						});

						describe('When PUT /quest/{questId}/complete is requested by the quester to set the quest completed', () => {

							before(done => {
								server.inject({
									method: 'PUT',
									url: `/quest/${quest.questId}/complete`,
									headers: {
										authorization: 'Bearer ' + otherJwt
									}
								}, response => {
									status = response.statusCode;
									done();
								});
							});

							it('should return a 204', () => {
								expect(status).to.equal(204);
							});

							describe('When GET /quest/{id} is requested', () => {

								let quest2;
								before(done => {
									server.inject({
										method: 'GET',
										url: `/quest/${quest.questId}`,
										headers: {
											authorization: 'Bearer ' + jwt
										}
									}, response => {
										quest2 = JSON.parse(response.payload);
										status = response.statusCode;
										done();
									});
								});

								it('should return a 200', () => {
									expect(status).to.equal(200);
								});

								it('should return a quest waiting on quest giver', () => {
									expect(quest2.status).to.equal('WAITING_ON_QUEST_GIVER');
								});

								describe('When PUT /quest/{questId}/complete is requested by the quest giver to set the quest completed', () => {

									before(done => {
										server.inject({
											method: 'PUT',
											url: `/quest/${quest.questId}/complete`,
											headers: {
												authorization: 'Bearer ' + jwt
											}
										}, response => {
											status = response.statusCode;
											done();
										});
									});

									it('should return a 204', () => {
										expect(status).to.equal(204);
									});

									describe('When GET /quest/{id} is requested', () => {

										let quest3;
										before(done => {
											server.inject({
												method: 'GET',
												url: `/quest/${quest.questId}`,
												headers: {
													authorization: 'Bearer ' + jwt
												}
											}, response => {
												quest3 = JSON.parse(response.payload);
												status = response.statusCode;
												done();
											});
										});

										it('should return a 200', () => {
											expect(status).to.equal(200);
										});

										it('should return a quest completed', () => {
											expect(quest3.status).to.equal('COMPLETED');
										});

										describe('When PUT /quest/{questId}/rate is requested by the quest giver to rate the quest', () => {

											before(done => {
												server.inject({
													method: 'PUT',
													url: `/quest/${quest.questId}/rate`,
													headers: {
														authorization: 'Bearer ' + jwt
													},
													payload: {
														rate: 1,
														comment: 'He did a bad job'
													}
												}, response => {
													status = response.statusCode;
													done();
												});
											});

											it('should return a 204', () => {
												expect(status).to.equal(204);
											});

											describe('When GET /quest/{id} is requested', () => {

												let quest5;
												before(done => {
													server.inject({
														method: 'GET',
														url: `/quest/${quest.questId}`,
														headers: {
															authorization: 'Bearer ' + jwt
														}
													}, response => {
														quest5 = JSON.parse(response.payload);
														status = response.statusCode;
														done();
													});
												});

												it('should return a 200', () => {
													expect(status).to.equal(200);
												});

												it('should return a quest completed', () => {
													expect(quest5.status).to.equal('COMPLETED');
												});

												it('should return a quest with ratings', () => {
													expect(quest5.receiverRating).to.equal(1);
												});

												it('should return a quest with comments', () => {
													expect(quest5.receiverComment).to.be.ok;
												});
											});

											describe('When PUT /quest/{questId}/rate is requested by the quester to rate the quest', () => {

												before(done => {
													server.inject({
														method: 'PUT',
														url: `/quest/${quest.questId}/rate`,
														headers: {
															authorization: 'Bearer ' + otherJwt
														},
														payload: {
															rate: 5,
															comment: 'He was friendly, glad I worked for him'
														}
													}, response => {
														status = response.statusCode;
														done();
													});
												});

												it('should return a 204', () => {
													expect(status).to.equal(204);
												});

												describe('When GET /quest/{id} is requested', () => {

													let quest4;
													before(done => {
														server.inject({
															method: 'GET',
															url: `/quest/${quest.questId}`,
															headers: {
																authorization: 'Bearer ' + jwt
															}
														}, response => {
															quest4 = JSON.parse(response.payload);
															status = response.statusCode;
															done();
														});
													});

													it('should return a 200', () => {
														expect(status).to.equal(200);
													});

													it('should return a quest completed', () => {
														expect(quest4.status).to.equal('COMPLETED');
													});

													it('should return a quest with ratings', () => {
														expect(quest4.giverRating).to.equal(5);
														expect(quest4.receiverRating).to.equal(1);
													});

													it('should return a quest with comments', () => {
														expect(quest4.giverComment).to.be.ok;
														expect(quest4.receiverComment).to.be.ok;
													});
												});
											});
										});

										describe('When GET /user/{id} is requested to check updated level', () => {

											let getUser;
											before(done => {
												server.inject({
													method: 'GET',
													url: `/user/${otherUserId}`,
													headers: {
														authorization: 'Bearer ' + jwt
													}
												}, response => {
													getUser = JSON.parse(response.payload);
													status = response.statusCode;
													done();
												});
											});

											it('should return a 200', () => {
												expect(status).to.equal(200);
											});

											it('should return a user that has a level above 1', () => {
												console.log('user level', getUser.level);
												expect(getUser.level).to.be.above(1);
											});
										});
									});
								});

								describe('When DELETE /quest/{id} is requested', () => {

									before(done => {
										server.inject({
											method: 'DELETE',
											url: `/quest/${quest.questId}`,
											headers: {
												authorization: 'Bearer ' + jwt
											}
										}, response => {
											status = response.statusCode;
											done();
										});
									});

									it('should return a 204', () => {
										expect(status).to.equal(204);
									});

									describe('When DELETE /user is requested', () => {

										before(done => {
											server.inject({
												method: 'DELETE',
												url: '/user',
												headers: {
													authorization: 'Bearer ' + jwt
												}
											}, response => {
												status = response.statusCode;
												server.inject({
													method: 'DELETE',
													url: '/user',
													headers: {
														authorization: 'Bearer ' + otherJwt
													}
												}, response => {
													status = response.statusCode;
													done();
												});
											});
										});

										it('should return a 204', () => {
											expect(status).to.equal(204);
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
});
