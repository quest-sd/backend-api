const expect = require('chai').expect;
const server = require('../../index');
const fixtures = require('../fixtures');
const defaults = require('../../config/quest').defaults;
const responses = require('../../config/swagger').responses;

let status;

describe('When GET /city is requested', () => {

	let cities;
	before(done => {
		server.inject({
			method: 'GET',
			url: '/city'
		}, response => {
			cities = JSON.parse(response.payload);
			status = response.statusCode;
			done();
		});
	});

	it('should return a 200', () => {
		expect(status).to.equal(200);
	});

	it('should return a list with at least one city', () => {
		expect(cities.length).to.be.at.least(1);
	});

	describe('When POST /user is requested', () => {

		const user = {
			name: fixtures.name,
			email: fixtures.email,
			password: fixtures.password,
			roleId: fixtures.roleId,
			profileImageId: fixtures.profileImageId
		};

		let createdUser;
		before(done => {
			user.cityId = cities[0].id;
			server.inject({
				method: 'POST',
				url: '/user',
				payload: user
			}, response => {
				createdUser = JSON.parse(response.payload);
				status = response.statusCode;
				done();
			});
		});

		it('should return a 201', () => {
			expect(status).to.equal(201);
		});

		it('should return the created user', () => {
			expect(createdUser.name).to.equal(fixtures.name);
			expect(createdUser.email).to.equal(fixtures.email);
			expect(createdUser.level).to.equal(defaults.level);
			expect(createdUser.exp).to.equal(defaults.exp);
			expect(createdUser.lastLogin).to.equal(createdUser.joined);
			expect(createdUser.cityId).to.equal(cities[0].id);
			expect(createdUser.roleId).to.equal(fixtures.roleId);
			expect(createdUser.profileImageId).to.equal(fixtures.profileImageId);
		});

		describe('When POST /user is requested with the same email', () => {

			let payload;
			before(done => {
				server.inject({
					method: 'POST',
					url: '/user',
					payload: user
				}, response => {
					payload = JSON.parse(response.payload);
					status = response.statusCode;
					done();
				});
			});

			it('should return a 409', () => {
				expect(status).to.equal(409);
			});

			it('should have a message saying that the user is a duplicate', () => {
				expect(payload.message).to.equal(responses.user.post.responses['409'].description);
			});
		});

		describe('When POST /login is required with invalid credentials', () => {

			const credentials = {
				email: user.email,
				password: "badpass"
			};

			let payload;
			before(done => {
				server.inject({
					method: 'POST',
					url: '/login',
					payload: credentials
				}, response => {
					payload = JSON.parse(response.payload);
					status = response.statusCode;
					done();
				});
			});

			it('should return a 401', () => {
				expect(status).to.equal(401);
			});

			it('should have a message saying that credentials are invalid', () => {
				expect(payload.message).to.equal(responses.login.post.responses['401'].description);
			});
		});

		describe('When POST /login is required with valid credentials', () => {

			const credentials = {
				email: user.email,
				password: user.password
			};

			let jwt;
			let userId;
			before(done => {
				server.inject({
					method: 'POST',
					url: '/login',
					payload: credentials
				}, response => {
					jwt = JSON.parse(response.payload).jwt;
					userId = JSON.parse(response.payload).userId;
					status = response.statusCode;
					done();
				});
			});

			it('should return a 200', () => {
				expect(status).to.equal(200);
			});

			it('should return a jwt token', () => {
				expect(jwt.length).to.be.above(20);
			});

			describe('When POST /login/email is requested with valid email and key', () => {

				const credentials = {
					email: user.email,
					key: fixtures.publicKey
				};

				let jwt;
				let userId;
				before(done => {
					server.inject({
						method: 'POST',
						url: '/login/email',
						payload: credentials
					}, response => {
						jwt = JSON.parse(response.payload).jwt;
						userId = JSON.parse(response.payload).userId;
						status = response.statusCode;
						done();
					});
				});

				it('should return a 200', () => {
					expect(status).to.equal(200);
				});

				it('should return a jwt token', () => {
					expect(jwt.length).to.be.above(20);
				});
			});

			describe('When PUT /user is requested', () => {

				const update = {
					name: 'Ricky Vasquez',
					email: 'ricardov@knights.ucf.edu'
				};

				before(done => {
					server.inject({
						method: 'PUT',
						url: '/user',
						headers: {
							authorization: 'Bearer ' + jwt
						},
						payload: update
					}, response => {
						status = response.statusCode;
						done();
					});
				});

				it('should return a 204', () => {
					expect(status).to.equal(204);
				});

				describe('When GET /user/{id} is requested', () => {

					let getUser;
					before(done => {
						server.inject({
							method: 'GET',
							url: `/user/${userId}`,
							headers: {
								authorization: 'Bearer ' + jwt
							}
						}, response => {
							getUser = JSON.parse(response.payload);
							status = response.statusCode;
							done();
						});
					});

					it('should return a 200', () => {
						expect(status).to.equal(200);
					});

					it('should return valid user', () => {
						expect(getUser.password).to.not.exist;
						expect(getUser.name).to.equal(update.name);
						expect(getUser.email).to.equal(update.email);
						expect(getUser.level).to.equal(1);
					});
				});
			});

			describe('When DELETE /user is requested', () => {

				before(done => {
					server.inject({
						method: 'DELETE',
						url: '/user',
						headers: {
							authorization: 'Bearer ' + jwt
						}
					}, response => {
						status = response.statusCode;
						done();
					});
				});

				it('should return a 204', () => {
					expect(status).to.equal(204);
				});
			});
		});
	});
});
