const expect = require('chai').expect;
const server = require('../../index');
const fixtures = require('../fixtures');
const mysql = require('mysql');
const formData = require('form-data');
const streamToPromise = require('stream-to-promise');
const fs = require('fs');

var env = process.env.NODE_ENV || 'development';
var config = require('../../config/database.json')[env];

var status;

describe('When POST /image is requested', () => {

    var image;
    before(done => {
        var form = new formData();
        form.append('file', fs.createReadStream(__dirname + '/../data/test.png'));
        var headers = form.getHeaders();
        streamToPromise(form).then((payload) => {
            server.inject({
                method: 'POST',
                url: '/image',
                headers: headers,
                payload: payload
            }, response => {
                status = response.statusCode;
                image = JSON.parse(response.payload);
                done();
            });
        });
    });

    after(done => {
        var connection = mysql.createConnection({
            host: config.host,
            user: config.username,
            password: config.password,
            database: config.database
        });
        var deleteQuery = `DELETE FROM IMAGES WHERE IMAGE_ID = '${image.id}'`;
        connection.query(deleteQuery, (err, result) => {
            done();
        });
    });

    it('should return 201', () => {
        expect(status).to.equal(201);
    });

    describe('When GET /image/{imageId} is requested', () => {

        var getImage;
        before(done => {
            server.inject({
                method: 'GET',
                url: '/image/' + image.id
            }, response => {
                getImage = JSON.parse(response.payload);
                status = response.statusCode;
                done();
            });
        });

        it('should return 200', () => {
            expect(status).to.equal(200);
        });

        it('should return image filename', () => {
            expect(getImage.filename).to.not.be.empty;
        });

        describe('When GET /image/file/{filename} is requested', () => {

            before(done => {
                server.inject({
                    method: 'GET',
                    url: '/image/file/' + getImage.filename
                }, response => {
                    status = response.statusCode;
                    done();
                });
            });

            it('should return 200', () => {
                expect(status).to.equal(200);
            });
        });
    });
});
