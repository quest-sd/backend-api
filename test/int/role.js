const expect = require('chai').expect;
const server = require('../../index');
const mysql = require('mysql');
const fixtures = require('../fixtures');

const env = process.env.NODE_ENV || 'development';
const config = require('../../config/database.json')[env];

let status;

describe('When GET /role is requested', () => {

	let roles;
	before(done => {
		server.inject({
			method: 'GET',
			url: '/role'
		}, response => {
			roles = JSON.parse(response.payload);
			status = response.statusCode;
			done();
		});
	});

	it('should return a 200', () => {
		expect(status).to.equal(200);
	});

	it('should return a list with at least one role', () => {
		expect(roles.length).to.be.at.least(1);
	});

	describe('When POST /role is requested', () => {

		const user = {
			name: fixtures.name,
			email: fixtures.email,
			password: fixtures.password,
			cityId: fixtures.cityId,
			profileImageId: fixtures.profileImageId
		};

		let createdUser;
		before(done => {
			user.roleId = roles[0].id;
			server.inject({
				method: 'POST',
				url: '/user',
				payload: user
			}, response => {
				createdUser = JSON.parse(response.payload);
				status = response.statusCode;
				done();
			});
		});

		let jwt;
		const credentials = {
			email: fixtures.email,
			password: fixtures.password
		};
		before(done => {
			server.inject({
				method: 'POST',
				url: '/login',
				payload: credentials
			}, response => {
				jwt = JSON.parse(response.payload).jwt;
				status = response.statusCode;
				done();
			});
		});

		const role = {
			name: 'TESTROLE'
		};
		let createdRole;
		before(done => {
			server.inject({
				method: 'POST',
				url: '/role',
				payload: role,
				headers: {
					authorization: 'Bearer ' + jwt
				}
			}, response => {
				createdRole = JSON.parse(response.payload);
				status = response.statusCode;
				done();
			});
		});

		after(done => {
			server.inject({
				method: 'DELETE',
				url: '/user',
				headers: {
					authorization: 'Bearer ' + jwt
				}
			}, response => {
				status = response.statusCode;
				done();
			});
		});

		after(done => {
			const connection = mysql.createConnection({
				host: config.host,
				user: config.username,
				password: config.password,
				database: config.database
			});
			const deleteQuery = `DELETE FROM ROLES WHERE ROLE_ID = '${createdRole.id}'`;
			connection.query(deleteQuery, (err, result) => {
				done();
			});
		});

		it('should return a 201', () => {
			expect(status).to.equal(201);
		});

		it('should return the created role', () => {
			expect(createdRole.name).to.equal(role.name);
		});
	});
});
