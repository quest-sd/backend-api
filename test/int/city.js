const expect = require('chai').expect;
const server = require('../../index');
const fixtures = require('../fixtures');
const mysql = require('mysql');

var env = process.env.NODE_ENV || 'development';
var config = require('../../config/database.json')[env];

var status;

describe('When GET /city is requested', () => {

    var count;
    var cities;
    before(done => {
        server.inject({
            method: 'GET',
            url: '/city'
        }, response => {
            cities = JSON.parse(response.payload);
            count = cities.length;
            status = response.statusCode;
            done();
        });
    });

    it('should return a 200', () => {
        expect(status).to.equal(200);
    });

    it('should return a list with at least one city', () => {
        expect(cities.length).to.be.at.least(1);
    });

    describe('When POST /city is requested', () => {

        var user = {
            name: fixtures.name,
            email: fixtures.email,
            password: fixtures.password,
            roleId: fixtures.roleId,
            profileImageId: fixtures.profileImageId
        };

        var createdUser;
        before(done => {
            user.cityId = cities[0].id;
            server.inject({
                method: 'POST',
                url: '/user',
                payload: user
            }, response => {
                createdUser = JSON.parse(response.payload);
                status = response.statusCode;
                done();
            });
        });

        const credentials = {
            email: fixtures.email,
            password: fixtures.password
        };

        var jwt;
        before(done => {
            server.inject({
                method: 'POST',
                url: '/login',
                payload: credentials
            }, response => {
                jwt = JSON.parse(response.payload).jwt;
                status = response.statusCode;
                done();
            });
        });

        var createdCity;
        const city = {
            name: 'TESTCITY' 
        };
        before(done => {
            server.inject({
                method: 'POST',
                url: '/city',
                payload: city,
                headers: {
                    authorization: 'Bearer ' + jwt
                }
            }, response => {
                createdCity = JSON.parse(response.payload);
                status = response.statusCode;
                done();
            });
        });

        after(done => {
            server.inject({
                method: 'DELETE',
                url: '/user',
                headers: {
                    authorization: 'Bearer ' + jwt
                }
            }, response => {
                status = response.statusCode;
                done();
            });
        });

        it('should return a 201', () => {
            expect(status).to.equal(201);
        });

        it('should return the created city', () => {
            expect(createdCity.name).to.equal(city.name);
        });

        describe('When PUT /city is requested', () => {

            var updatedCity;
            var updateCity = {
                name: 'UPDATECITY',
            };
            before(done => {
                updateCity.id = createdCity.id;
                server.inject({
                    method: 'PUT',
                    url: '/city',
                    payload: updateCity,
                    headers: {
                        authorization: 'Bearer ' + jwt
                    }
                }, response => {
                    updatedCity = JSON.parse(response.payload);
                    status = response.statusCode;
                    done();
                });
            });

            it('should return a 200', () => {
                expect(status).to.equal(200);
            });

            it('should return the updated city', () => {
                expect(updatedCity.name).to.equal(updateCity.name);
            });

            describe('When GET /city is requested', () => {

                var newCount;
                var updatedCities;
                before(done => {
                    server.inject({
                        method: 'GET',
                        url: '/city'
                    }, response => {
                        updatedCities = JSON.parse(response.payload);
                        newCount = updatedCities.length;
                        status = response.statusCode;
                        done();
                    });
                });

                after(done => {
                    var connection = mysql.createConnection({
                        host: config.host,
                        user: config.username,
                        password: config.password,
                        database: config.database
                    });
                    var deleteQuery = `DELETE FROM CITIES WHERE CITY_ID = '${createdCity.id}'`;
                    connection.query(deleteQuery, (err, result) => {
                        done();
                    });
                });

                it('should return a 200', () => {
                    expect(status).to.equal(200);
                });

                it('should return a list with a count of +1 city', () => {
                    expect(count + 1).to.equal(newCount);
                });
            });
        });
    });
});
