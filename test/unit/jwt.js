const expect = require('chai').expect;
const jsonwebtoken = require('jsonwebtoken');
const jwtBuilder = require('../../app/helpers/jwt');
const fixtures = require('../fixtures');
const fs = require('fs');
const key = fs.readFileSync('private_key.pem');

describe('When jwt is built from user object', () => {
    const user = {
        userId: fixtures.userId,
        email: fixtures.email,
        name: fixtures.name,
        password: fixtures.password
    };

    var jwt;
    before(done => {
        jwt = jwtBuilder.login(user);
        done();
    });

    it('should serialize user object into a jwt string', () => {
        expect(jwt.length).to.be.above(20);
    });

    describe('When jwt is deserialized', () => {
        var deserializedToken;
        before(done => {
            deserializedToken = jsonwebtoken.verify(jwt, key, done());
        });

        it('should contain userId and email', () => {
            expect(deserializedToken.userId).to.equal(user.userId);
            expect(deserializedToken.email).to.equal(user.email);
        });
    });
});
