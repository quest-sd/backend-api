module.exports = {
	statuses: {
		completed: 'COMPLETED',
		inProgress: 'IN_PROGRESS',
		awaitingQuestGiver: 'WAITING_ON_QUEST_GIVER',
		open: 'OPEN',
		abandoned: 'ABANDONED'
	}
};
