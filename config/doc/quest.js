const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
	post: Joi.object({
		name: Joi.string().required().example(fixtures.questName),
		rewardItemId: Joi.string().example(fixtures.itemId),
		description: Joi.string().example(fixtures.questDescription),
		address: Joi.string().required().example(fixtures.questAddress),
		latitude: Joi.number().example(fixtures.questLatitude),
		longitude: Joi.number().example(fixtures.questLongitude),
		cashReward: Joi.number().example(fixtures.questCashReward)
	}),
	getQuest: Joi.object({
		name: Joi.string().example(fixtures.questName),
		rewardImageId: Joi.string().example(fixtures.questRewardImageId),
		rewardExp: Joi.string().example(fixtures.questRewardExp),
		rewardDescription: Joi.string().example(fixtures.questRewardDescription),
		rewardName: Joi.string().example(fixtures.itemName),
		rewardCondition: Joi.string().example(fixtures.itemCondition),
		rewardItemId: Joi.string().example(fixtures.itemId),
		rewardDateAdded: Joi.date().iso().example(fixtures.questCreated),
		description: Joi.string().example(fixtures.questDescription),
		address: Joi.string().example(fixtures.questAddress),
		latitude: Joi.number().example(fixtures.questLatitude),
		longitude: Joi.number().example(fixtures.questLongitude),
		created: Joi.date().iso().example(fixtures.questCreated),
		completed: Joi.date().iso().example(fixtures.questCompleted),
		questId: Joi.string().example(fixtures.questId),
		receiverId: Joi.string().example(fixtures.questReceiverId),
		giverId: Joi.string().example(fixtures.questGiverId),
		status: Joi.string().example(fixtures.questStatus),
		giverRating: Joi.number().example(fixtures.questGiverRating),
		receiverRating: Joi.number().example(fixtures.questReceiverRating),
		cashReward: Joi.number().example(fixtures.questCashReward),
		receiverComment: Joi.string().example(fixtures.questReceiverComment),
		giverComment: Joi.string().example(fixtures.questGiverComment),
		applicants: Joi.array().items(Joi.string()).example(fixtures.questApplicants)
	}),
	get: Joi.array().items(Joi.object({
		questId: Joi.string().example(fixtures.questId),
		receiverId: Joi.string().example(fixtures.questReceiverId),
		giverId: Joi.string().example(fixtures.questGiverId),
		name: Joi.string().example(fixtures.questName),
		tags: Joi.array().items(Joi.string()).example(fixtures.tags)
	})),
	rate: Joi.object({
		rate: Joi.number().min(1).max(5).example(fixtures.questReceiverRating),
		comment: Joi.string().example(fixtures.questGiverComment)
	}),
	put: Joi.object({
		name: Joi.string().example(fixtures.questName),
		rewardItemId: Joi.string().example(fixtures.questRewardImageId),
		description: Joi.string().example(fixtures.questDescription),
		address: Joi.string().example(fixtures.questAddress),
		latitude: Joi.number().example(fixtures.questLatitude),
		longitude: Joi.number().example(fixtures.questLongitude),
		cashReward: Joi.number().example(fixtures.questCashReward)
	}),
	id: Joi.string().example(fixtures.questId),
	tagId: Joi.string().example(fixtures.tagId),
};

const schema = {
	post: {
		payload: joi.post,
		response: joi.getQuest
	},
	apply: {
		param: joi.id,
	},
	accept: {
		param: joi.id,
	},
	rate: {
		payload: joi.rate
	},
	put: {
		payload: joi.put,
		param: joi.id,
		response: joi.getQuest
	},
	delete: {
		param: joi.id
	},
	get: {
		response: joi.get
	},
	getByTag: {
		param: joi.tagId,
		response: joi.get
	},
	getByUserId: {
		param: joi.tagId,
		response: joi.get
	},
	getQuest: {
		param: joi.id,
		response: joi.getQuest
	},
	tagQuest: {
		tag: joi.tagId,
		quest: joi.id
	},
	deleteTagQuest: {
		tag: joi.tagId,
		quest: joi.id
	}
};

const responses = {
	post: {
		security: [{ 'jwt': [] }],
		responses: {
			'201': {
				description: 'Quest successfully created.',
				schema: schema.post.response
			},
			'500': {
				description: 'Error creating quest.',
				schema: Joi.string()
			}
		}
	},
	apply: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'User applied to Quest successfully.'
			},
			'500': {
				description: 'Error updating quest.',
				schema: Joi.string()
			}
		}
	},
	accept: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'User accepted to Quest successfully.'
			},
			'500': {
				description: 'Error updating quest.',
				schema: Joi.string()
			}
		}
	},
	complete: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'Quest completed.'
			},
			'500': {
				description: 'Error updating quest.',
				schema: Joi.string()
			}
		}
	},
	abandon: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'Quester abandoned.'
			},
			'500': {
				description: 'Error updating quest.',
				schema: Joi.string()
			}
		}
	},
	rate: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'Quest rated.'
			},
			'500': {
				description: 'Error updating quest.',
				schema: Joi.string()
			}
		}
	},
	put: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'Quest successfully updated.'
			},
			'500': {
				description: 'Error updating quest.',
				schema: Joi.string()
			}
		}
	},
	delete: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'Quest successfully deleted.'
			},
			'500': {
				description: 'Error deleting quest.',
				schema: Joi.string()
			}
		}
	},
	get: {
		security: [{ 'jwt': [] }],
		responses: {
			'200': {
				description: 'Quests successfully returned.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error getting quests.',
				schema: Joi.string()
			}

		}
	},
	getByTag: {
		security: [{ 'jwt': [] }],
		responses: {
			'200': {
				description: 'Quests successfully returned.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error getting quests.',
				schema: Joi.string()
			}

		}
	},
	getByUserId: {
		security: [{ 'jwt': [] }],
		responses: {
			'200': {
				description: 'Quests successfully returned.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error getting quests.',
				schema: Joi.string()
			}

		}
	},
	getQuest: {
		security: [{ 'jwt': [] }],
		responses: {
			'200': {
				description: 'Quest successfully returned.',
				schema: schema.getQuest.response
			},
			'500': {
				description: 'Error getting quest.',
				schema: Joi.string()
			}

		}
	},
	tagQuest: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'Quest successfully tagged.'
			},
			'500': {
				description: 'Error getting quest.',
				schema: Joi.string()
			}

		}
	},
	deleteTagQuest: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'Tag successfully deleted from quest.'
			},
			'500': {
				description: 'Error getting quest.',
				schema: Joi.string()
			}
		}
	}
};

const route = {
	tags: ['api'],
	post: {
		description: 'Create new Quest',
		notes: [
			'Create a new Quest.',
			'Quest name and address are required',
			'Quest status is set to OPEN upon creation by the backend.'
		]
	},
	apply: {
		description: 'Apply for a Quest',
		notes: [
			'Adds the userId in the path param to the list of quest applicants.',
			'Only accessible if the quest has a `status` of `OPEN`'
		]
	},
	accept: {
		description: 'Accept Quester for a Quest',
		notes: [
			'Makes the userId in the path param the receiverId of the quest.',
			'Only accessible if the quest has a `status` of `OPEN`',
			'Quester userId must be in the list of applicants',
			'Changes the `status` of the Quest to `IN_PROGRESS`'
		]
	},
	complete: {
		description: 'Complete a Quest',
		notes: [
			'The Quest Giver can make this request without the Quester.',
			'If a Quester makes this request the `status` of the Quest is changed to `WAITING_ON_QUEST_GIVER`.',
			'If a Quest Giver makes this request the `status` of the Quest is changed to `COMPLETED`.',
			'Only accessible if the quest has a `status` of `IN_PROGRESS` or `WAITING_ON_QUEST_GIVER`.',
			'Quester userId must be be in the `receiverId`.'
		]
	},
	abandon: {
		description: 'Abandon a Quest',
		notes: [
			'Removes the Quester from the quest and sets the `status` to `OPEN`.',
			'Only accessible if the Quest has a `status` of `IN_PROGRESS`.',
		]
	},
	rate: {
		description: 'Rate a Quest',
		notes: [
			'Either Quester or Quest Giver can make this request. The rating will be applied appropriately.',
			'Only accessible if the Quest has a `status` of `COMPLETED`.',
		]
	},
	put: {
		description: 'Update a Quest',
		notes: [
			'Omitted fields will NOT be updated.',
			'Only a Quest description or address can be updated when the status is not `OPEN`.'
		]
	},
	delete: {
		description: 'Deletes a Quest',
		notes: [
			'Deletes a Quest created by a user and has not yet been accepted.',
			'The Quest must have a status of `OPEN` to be deleted by the quest giver.'
		]
	},
	get: {
		description: 'Get all Quests',
		notes: [
			'Returns a list of Quests in the user\'s city.',
			`Quests are returned in an array of objects containing questId, giverId, and receiverId.`
		]
	},
	getByTag: {
		description: 'Get all Quests by Tag',
		notes: [
			'Returns a list of Quests in the user\'s city by tag.',
			'The tag ID must first exist in the system. See POST /tag'
		]
	},
	getByUserId: {
		description: 'Get all Quests by UserId',
		notes: [
			'Returns a list of Quests in the user\'s city where they are the quest giver or quest receiver.'
		]
	},
	getQuest: {
		description: 'Get quest details',
		notes: [
			'Returns a Quest\'s details.'
		]
	},
	tagQuest: {
		description: 'Tag a quest',
		notes: [
			'Tag a quest using a tag ID.'
		]
	},
	deleteTagQuest: {
		description: 'Remove tag from quest',
		notes: [
			'Remove tag from quest using a tag ID.'
		]
	}
};

module.exports = {
	route,
	joi,
	schema,
	responses
};
