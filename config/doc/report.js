const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
	create: Joi.object({
		reporterId: Joi.string().required().example(fixtures.userId),
		offenderId: Joi.string().required().example(fixtures.userId),
		reason: Joi.string().required().example(fixtures.reportReason),
		message: Joi.string().example(fixtures.reportMessage),
		questId: Joi.string().example(fixtures.questId),
	}),
	getOne: Joi.object({
		reportId: Joi.string().example(fixtures.itemId),
		reporterId: Joi.string().example(fixtures.userId),
		offenderId: Joi.string().example(fixtures.questGiverId),
		status: Joi.string().example(fixtures.unresolved),
		timestamp: Joi.date().iso().example(fixtures.questCompleted),
		reason: Joi.string().example(fixtures.reportReason),
		message: Joi.string().example(fixtures.reportMessage),
		questId: Joi.string().example(fixtures.questId),
	}),
	get: Joi.array().items(Joi.object({
		reportId: Joi.string().example(fixtures.itemId),
		reporterId: Joi.string().example(fixtures.userId),
		offenderId: Joi.string().example(fixtures.questGiverId),
		status: Joi.string().example(fixtures.resolved),
		timestamp: Joi.date().iso().example(fixtures.questCompleted),
		reason: Joi.string().example(fixtures.reportReason),
		message: Joi.string().example(fixtures.reportMessage),
		questId: Joi.string().example(fixtures.questId),
	})),
	id: Joi.string().example(fixtures.imageId)
};

const schema = {
	post: {
		payload: joi.create,
		response: joi.getOne
	},
	get: {
		response: joi.get
	},
	put: {
		param: joi.id,
	},
};

const responses = {
	post: {
		security: [{ 'jwt': [] }],
		responses: {
			'201': {
				description: 'Report was successfully added.',
				schema: schema.post.response
			},
			'500': {
				description: 'Error adding report to database.',
				schema: Joi.string()
			}
		}
	},
	get: {
		security: [{ 'jwt': [] }],
		responses: {
			'200': {
				description: 'Reports successfully returned.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error retrieving reports from database.',
				schema: Joi.string()
			}
		}
	},
	put: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'Report successfully updated.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error retrieving reports from database.',
				schema: Joi.string()
			}
		}
	}
};

const route = {
	tags: ['api'],
	post: {
		description: 'Create report',
		notes: ['Create a new report.']
	},
	get: {
		description: 'Get Reports',
		notes: ['Returns all reports.']
	},
	put: {
		description: 'Resolve Report',
		notes: ['Set a report to resolved.']
	}
};

module.exports = {
	route,
	joi,
	schema,
	responses
};
