const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
    create: Joi.object({
        name: Joi.string().required().example(fixtures.cityName)
    }),
    getOne: Joi.object({
        name: Joi.string().example(fixtures.cityName),
        id: Joi.string().example(fixtures.cityId)
    }),
    get: Joi.array().items(Joi.object({
        name: Joi.string().example(fixtures.cityName),
        id: Joi.string().example(fixtures.cityId)
    })),
    put: Joi.object({
        name: Joi.string().example(fixtures.cityName),
        id: Joi.string().example(fixtures.cityId)
    }),
};

const schema = {
    post: {
        payload: joi.create,
        response: joi.getOne
    },
    put: {
        payload: joi.put,
        response: joi.getOne
    },
    get: {
        response: joi.get
    }
};

const responses = {
    post: {
        security: [{ 'jwt': [] }],
        responses: {
            '201': {
                description: 'City was successfully added.',
                schema: schema.post.response
            },
            '409': {
                description: 'City with this name already exists.',
                schema: Joi.string()
            },
            '500': {
                description: 'Error adding city to database.',
                schema: Joi.string()
            }
        }
    },
    put: {
        security: [{ 'jwt': [] }],
        responses: {
            '200': {
                description: 'City was successfully updated.',
                schema: schema.put.response
            },
            '500': {
                description: 'Error updating city.',
                schema: Joi.string()
            }
        }
    },
    get: {
        responses: {
            '200': {
                description: 'Cities successfully returned.',
                schema: schema.get.response
            },
            '500': {
                description: 'Error retrieving cities.',
                schema: Joi.string()
            }
        }
    }
};

const route = {
    tags: ['api'],
    post: {
        description: 'Add City',
        notes: ['Add a new City. Returns the city\'s ID']
    },
    get: {
        description: 'Get avaliable cities',
        notes: ['Returns all cities.']
    },
    put: {
        description: 'Update City',
        notes: ['Update the city\'s name.']
    }
};

module.exports = {
    route,
    joi,
    schema,
    responses
};
