const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
	default: Joi.object({
		email: Joi.string().required().example(fixtures.email),
		password: Joi.string().required().example(fixtures.password)
	}),
	email: Joi.object({
		email: Joi.string().required().example(fixtures.email),
		key: Joi.string().required().example(fixtures.publicKey)
	}),
	jwt: Joi.object({
		jwt: Joi.string().example(fixtures.jwt),
		userId: Joi.string().example(fixtures.userId)
	})
};

const schema = {
	post: {
		payload: joi.default,
		response: joi.jwt
	},
	email: {
		payload: joi.email,
		response: joi.jwt
	}
};

const responses = {
	post: {
		responses: {
			'200': {
				description: 'JWT successfully created and returned.',
				schema: schema.post.response
			},
			'401': {
				description: 'Invalid email or password.',
				schema: Joi.string()
			},
			'500': {
				description: 'Error finding user.',
				schema: Joi.string()
			}
		}
	},
	email: {
		responses: {
			'200': {
				description: 'JWT successfully created and returned.',
				schema: schema.post.response
			},
			'401': {
				description: 'Invalid email or password.',
				schema: Joi.string()
			},
			'500': {
				description: 'Error finding user.',
				schema: Joi.string()
			}
		}
	}
};

const route = {
	tags: ['api'],
	post: {
		description: 'Sign in',
		notes: [
			'Sign in the user with an email and password.',
			'Returns a valid JWT to be used with other API requests.',
			'Also returns the current user\'s userId to be used in other API requests',
			'JWT bearer authorization is not required.'
		]
	},
	email: {
		description: 'Email Sign In',
		notes: [
			'Sign in the user with an email and key.',
			'Assumes the client has authorized the user via third party service.',
			'Public key must be encoded in base64.',
			'JWT bearer authorization is not required.'
		]
	}
};

module.exports = {
	route,
	joi,
	schema,
	responses
};
