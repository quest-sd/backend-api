const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
	create: Joi.object({
		name: Joi.string().required().example(fixtures.roleName)
	}),
	getOne: Joi.object({
		name: Joi.string().example(fixtures.roleName),
		id: Joi.string().example(fixtures.roleId)
	}),
	get: Joi.array().items(Joi.object({
		name: Joi.string().example(fixtures.roleName),
		id: Joi.string().example(fixtures.roleId)
	}))
};

const schema = {
	post: {
		payload: joi.create,
		response: joi.getOne
	},
	get: {
		response: joi.get
	}
};

const responses = {
	post: {
		security: [{ 'jwt': [] }],
		responses: {
			'201': {
				description: 'Role was successfully added.',
				schema: schema.post.response
			},
			'409': {
				description: 'Role name already exists.',
				schema: Joi.string()
			},
			'500': {
				description: 'Error adding role to database.',
				schema: Joi.string()
			}
		}
	},
	get: {
		responses: {
			'200': {
				description: 'Roles successfully returned.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error retrieving roles from database.',
				schema: Joi.string()
			}
		}
	}
};

const route = {
	tags: ['api'],
	post: {
		description: 'Create role',
		notes: ['Create a new role.']
	},
	get: {
		description: 'Get Roles',
		notes: ['Returns all roles.']
	}
};

module.exports = {
	route,
	joi,
	schema,
	responses
};
