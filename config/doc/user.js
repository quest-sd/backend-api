const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
	create: Joi.object({
		name: Joi.string().required().example(fixtures.name),
		email: Joi.string().required().example(fixtures.email),
		password: Joi.string().example(fixtures.password),
		cityId: Joi.string().required().example(fixtures.cityId),
		roleId: Joi.string().required().example(fixtures.roleId),
		profileImageId: Joi.string().example(fixtures.profileImageId),
		key: Joi.string().example(fixtures.publicKey)
	}),
	getUser: Joi.object({
		userId: Joi.string().example(fixtures.userId),
		name: Joi.string().example(fixtures.name),
		email: Joi.string().example(fixtures.email),
		level: Joi.number().example(1),
		exp: Joi.number().example(1),
		expToNextLevel: Joi.number().example(15),
		lastLogin: Joi.date().iso().example(fixtures.questCreated),
		joined: Joi.date().iso().example(fixtures.questCompleted),
		cityId: Joi.string().example(fixtures.cityId),
		roleId: Joi.string().example(fixtures.roleId),
		profileImageId: Joi.string().example(fixtures.profileImageId),
		health: Joi.number().example(5)
	}),
	put: Joi.object({
		name: Joi.string().example(fixtures.name),
		email: Joi.string().example(fixtures.email),
		cityId: Joi.string().example(fixtures.cityId),
		roleId: Joi.string().example(fixtures.roleId),
		profileImageId: Joi.string().example(fixtures.profileImageId)
	}),
	putById: Joi.object({
		health: Joi.number().example(1),
	}),
	get: Joi.array().items(Joi.object({
		userId: Joi.string().example(fixtures.userId),
		name: Joi.string().example(fixtures.name),
		level: Joi.number().example(1),
		profileImageId: Joi.string().example(fixtures.profileImageId)
	})),
	forgotPassword: Joi.object({
		email: Joi.string().example(fixtures.email)
	}),
	resetPassword: Joi.object({
		email: Joi.string().example(fixtures.email),
		tempPassword: Joi.string().example(fixtures.hashPassword),
		newPassword: Joi.string().example(fixtures.password)
	}),
	id: Joi.string().example(fixtures.userId)
};

const schema = {
	post: {
		payload: joi.create,
		response: joi.get
	},
	put: {
		payload: joi.put,
		response: joi.get
	},
	putById: {
		param: joi.id,
		payload: joi.putById
	},
	get: {
		response: joi.get
	},
	getUser: {
		param: joi.id,
		response: joi.getUser
	},
	forgotPassword: {
		payload: joi.forgotPassword
	},
	resetPassword: {
		payload: joi.resetPassword
	}
};

const responses = {
	resetPassword: {
		responses: {
			'204': {
				description: 'Password updated.'
			},
			'500': {
				description: 'Error updating user.',
				schema: Joi.string()
			}
		}
	},
	forgotPassword: {
		responses: {
			'204': {
				description: 'Temp password send.'
			},
			'500': {
				description: 'Error getting user.',
				schema: Joi.string()
			}
		}
	},
	post: {
		responses: {
			'201': {
				description: 'New user was successfully created.',
				schema: schema.post.response
			},
			'401': {
				description: 'User creation unauthorized.',
				schema: Joi.string()
			},
			'409': {
				description: 'User with this email already exists.',
				schema: Joi.string()
			},
			'500': {
				description: 'Error creating user.',
				schema: Joi.string()
			}
		}
	},
	put: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'User was successfully updated.'
			},
			'500': {
				description: 'Error updating user.',
				schema: Joi.string()
			}
		}
	},
	putById: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'User was successfully updated.'
			},
			'500': {
				description: 'Error updating user.',
				schema: Joi.string()
			}
		}
	},
	get: {
		security: [{ 'jwt': [] }],
		responses: {
			'200': {
				description: 'Users successfully returned.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error getting users.',
				schema: Joi.string()
			}
		}
	},
	getUser: {
		security: [{ 'jwt': [] }],
		responses: {
			'200': {
				description: 'User information was successfully returned.',
				schema: schema.getUser.response
			},
			'500': {
				description: 'Error getting user.',
				schema: Joi.string()
			}
		}
	},
	delete: {
		security: [{ 'jwt': [] }],
		responses: {
			'204': {
				description: 'User was successfully deleted.',
			}
		}
	}
};

const route = {
	tags: ['api'],
	post: {
		description: 'Create User',
		notes: [
            'Create a new user with a email, name, city, and password.',
            'Email must be unique in the system.',
            'When creating a user without a password, a public key is required in the `key` field',
            'JWT bearer authorization is not required.',
        ]
	},
	get: {
		description: 'Get Users',
		notes: ['Returns the users partial info']
	},
	getUser: {
		description: 'Get User by ID',
		notes: ['Returns the user\'s information.']
	},
	put: {
		description: 'Update User',
		notes: ['Update the user\'s email, name or city. Returns the user\'s information.']
	},
	putById: {
		description: 'Update User Health',
		notes: ['Update the user\'s health.']
	},
	delete: {
		description: 'Delete User',
		notes: ['Deletes the user and their information from the system.']
	},
	forgotPassword: {
		description: 'Forgot User',
		notes: ['Sends a temporary password to the user\'s email']
	},
	resetPassword: {
		description: 'Reset User Password',
		notes: ['Update users password using a temp password']
	}
};

module.exports = {
	route,
	joi,
	schema,
	responses
};
