const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
    create: {
        file: Joi.binary().required()
    },
    getId: Joi.object({
        id: Joi.string().example(fixtures.imageId),
        filename: Joi.string().example(fixtures.imageFilename)
    }),
    getIdParam: Joi.string().example(fixtures.imageId),
    getFilename: Joi.string().example(fixtures.imageFilename),
    getFile: Joi.binary()
};

const schema = {
    post: {
        payload: joi.create,
        response: joi.getId
    },
    get: {
        param: joi.getIdParam,
        response: joi.getId
    },
    getFile: {
        param: joi.getFilename,
        response: joi.getFile
    }
};

const responses = {
    post: {
        responses: {
            '201': {
                description: 'Image was successfully added.',
                schema: schema.post.response
            },
            '500': {
                description: 'Error uploading image.',
                schema: Joi.string()
            }
        },
        payloadType: 'form'
    },
    get: {
        responses: {
            '200': {
                description: 'Image filename successfully returned.',
                schema: schema.get.response
            },
            '500': {
                description: 'Error getting image id.',
                schema: Joi.string()
            }
        }
    },
    getFile: {
        responses: {
            '200': {
                description: 'Image successfully returned.',
                schema: schema.getFile.response
            },
            '500': {
                description: 'Error getting image id.',
                schema: Joi.string()
            }
        }
    }
};

const route = {
    tags: ['api'],
    post: {
        description: 'Upload image',
        notes: ['Upload a new image.']
    },
    get: {
        description: 'Get an image file path',
        notes: ['Return image file path.']
    },
    getFile: {
        description: 'Get an image file',
        notes: ['Return image file.']
    }
};

module.exports = {
    route,
    joi,
    schema,
    responses
};
