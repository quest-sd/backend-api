const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
	post: Joi.object({
		name: Joi.string().required().example(fixtures.itemName),
		imageId: Joi.string().example(fixtures.imageId),
        description: Joi.string().example(fixtures.itemDescription),
        condition: Joi.string().example(fixtures.itemCondition)
	}),
	getItem: Joi.object({
		itemId: Joi.string().example(fixtures.itemId),
		ownerId: Joi.string().example(fixtures.userId),
		imageId: Joi.string().example(fixtures.imageId),
		name: Joi.string().example(fixtures.itemName),
        dateAdded: Joi.date().iso().example(fixtures.itemDateAdded),
        description: Joi.string().example(fixtures.itemDescription),
        condition: Joi.string().example(fixtures.itemCondition)
	}),
	put: Joi.object({
		itemId: Joi.string().required().example(fixtures.itemId),
		ownerId: Joi.string().example(fixtures.userId),
		imageId: Joi.string().example(fixtures.imageId),
		name: Joi.string().example(fixtures.itemName),
        description: Joi.string().example(fixtures.itemDescription),
        condition: Joi.string().example(fixtures.itemCondition)
	}),
	get: Joi.array().items(Joi.object({
		itemId: Joi.string().example(fixtures.itemId),
		ownerId: Joi.string().example(fixtures.userId),
		imageId: Joi.string().example(fixtures.imageId),
		name: Joi.string().example(fixtures.itemName),
		dateAdded: Joi.date().iso().example(fixtures.itemDateAdded),
		description: Joi.string().example(fixtures.itemDescription),
		condition: Joi.string().example(fixtures.itemCondition)
	})),
	id: Joi.string().example(fixtures.imageId)
};

const schema = {
	post: {
		payload: joi.post,
		response: joi.getItem
	},
	put: {
		payload: joi.put
	},
	get: {
		response: joi.get
	},
	delete: {
		param: joi.id
	},
	getItem: {
		param: joi.id
	}
};

const responses = {
	post: {
		responses: {
			'201': {
				description: 'Item was successfully added.',
				schema: schema.post.response
			},
			'500': {
				description: 'Error adding item.',
				schema: Joi.string()
			}
		}
	},
	put: {
		responses: {
			'204': {
				description: 'Item was successfully updated.',
			},
			'500': {
				description: 'Error adding item.',
				schema: Joi.string()
			}
		}
	},
	get: {
		responses: {
			'200': {
				description: 'Items successfully returned.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error getting items.',
				schema: Joi.string()
			}
		}
	},
	getItem: {
		responses: {
			'200': {
				description: 'Item successfully returned.',
				schema: schema.getItem.response
			},
			'500': {
				description: 'Error getting item.',
				schema: Joi.string()
			}
		}
	},
	delete: {
		responses: {
			'204': {
				description: 'Item successfully removed.'
			},
			'500': {
				description: 'Error removing item.',
			}
		}
	}
};

const route = {
	tags: ['api'],
	post: {
		description: 'Add item',
		notes: ['Add a new item.']
	},
	get: {
		description: 'Get items',
		notes: ['Returns all users items']
	},
	getItem: {
		description: 'Get item',
		notes: ['Return item file path and details.']
	},
	delete: {
		description: 'Remove an item',
		notes: ['Removes an item.']
	},
	put: {
		description: 'Update an item',
		notes: ['Updates an item.']
	}
};

module.exports = {
	route,
	joi,
	schema,
	responses
};
