const Joi = require('joi');
const fixtures = require('../../test/fixtures');

const joi = {
	create: Joi.object({
		name: Joi.string().required().example(fixtures.tagName)
	}),
	getOne: Joi.object({
		name: Joi.string().example(fixtures.tagName),
		id: Joi.string().example(fixtures.tagId)
	}),
	get: Joi.array().items(Joi.object({
		name: Joi.string().example(fixtures.tagName),
		id: Joi.string().example(fixtures.tagId)
	}))
};

const schema = {
	post: {
		payload: joi.create,
		response: joi.getOne
	},
	get: {
		response: joi.get
	}
};

const responses = {
	post: {
		security: [{ 'jwt': [] }],
		responses: {
			'201': {
				description: 'Tag was successfully added.',
				schema: schema.post.response
			},
			'409': {
				description: 'Tag already exists.',
				schema: Joi.string()
			},
			'500': {
				description: 'Error creating tag.',
				schema: Joi.string()
			}
		}
	},
	get: {
		security: [{ 'jwt': [] }],
		responses: {
			'200': {
				description: 'Tags successfully returned.',
				schema: schema.get.response
			},
			'500': {
				description: 'Error getting tags.',
				schema: Joi.string()
			}
		}
	}
};

const route = {
	tags: ['api'],
	post: {
		description: 'Create tag',
		notes: ['Create a new tag.']
	},
	get: {
		description: 'Get Tags',
		notes: ['Returns all tags.']
	}
};

module.exports = {
	route,
	joi,
	schema,
	responses
};
