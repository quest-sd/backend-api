var env = process.env;

const config = {
	connection: {
		host: env.HOST || '0.0.0.0',
		port: env.PORT || 3000,
		routes: {
			cors: true
		}
	},
	key: env.PRIVATE_KEY || 'private_key.pem',
	uploadDir: env.UPLOAD_DIR || 'uploads',
	log: env.NODE_ENV === 'test-nolog' ? false : true,
	defaults: {
		level: 1,
		exp: 1
	},
	routes: {
		cors: true
	},
	expConstant: .5,
	appIdOneSignal: '611ac705-635c-4226-b59d-b0f80685eca0',
	keyOneSignal: 'OTMwMWJjNDItMGMxYy00OWU2LThjZWEtZTA2MzIwMDJiMWI2'
};

module.exports = config;
