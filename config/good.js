var good = {
    reporters: {
        console: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ log: '*', response: '*' }]
        }, {
            module: 'good-console'
        }, 'stdout']
    }
};

if (process.env.NODE_ENV === 'test-nolog') {
    good.reporters = {};
}

module.exports = good;
