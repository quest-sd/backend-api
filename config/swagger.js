const Joi = require('joi');
const fixtures = require('../test/fixtures');
const login = require('./doc/login');
const city = require('./doc/city');
const image = require('./doc/image');
const quest = require('./doc/quest');
const tag = require('./doc/tag');
const user = require('./doc/user');
const role = require('./doc/role');
const item = require('./doc/item');
const report = require('./doc/report');

const routes = {
	login: login.route,
	user: user.route,
	city: city.route,
	tag: tag.route,
	role: role.route,
	image: image.route,
	quest: quest.route,
	item: item.route,
	report: report.route
};

const schemas = {
	authorization:
		Joi.object({
			authorization: Joi.string().required().default('Bearer {insert jwt here}')
		}).unknown(),
	login: login.schema,
	user: user.schema,
	city: city.schema,
	tag: tag.schema,
	role: role.schema,
	image: image.schema,
	quest: quest.schema,
	item: item.schema,
	report: report.schema
};

const responses = {
	login: login.responses,
	user: user.responses,
	city: city.responses,
	tag: tag.responses,
	role: role.responses,
	image: image.responses,
	quest: quest.responses,
	item: item.responses,
	report: report.responses
};

const swagger = {
	options: {
		info: {
			title: 'Quest Backend API',
			description: 'API Documentation',
			version: require('./../package.json').version
		},
		securityDefinitions: {
			jwt: {
				type: 'apiKey',
				name: 'Authorization',
				in: 'header'
			}
		},
	},
	routes,
	schemas,
	responses
};

module.exports = swagger;
